#include <config.h>
#include "examples/writexdr.c"

int main ()
{
  if( SOLVERBENCH_VERSION != atof( FVCA6_SOLVER_BENCH_VERSION ) )
  {
    fprintf(stderr,"\nVersion of writexdr.c needs an update to %s \n\n", FVCA6_SOLVER_BENCH_VERSION );
    exit ( EXIT_FAILURE );
  }
  return EXIT_SUCCESS ;
}
