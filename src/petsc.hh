#ifndef DUNE_PETSC_HH
#define DUNE_PETSC_HH

/*
  Include "petscksp.h" so that we can use KSP solvers.  Note that this file
  automatically includes:
     petscsys.h       - base PETSc routines   petscvec.h - vectors
     petscmat.h - matrices
     petscis.h     - index sets            petscksp.h - Krylov subspace methods
     petscviewer.h - viewers               petscpc.h  - preconditioners
*/
#if HAVE_PETSC
#include "petscksp.h"
#else
#warning "PETSc was not found!"
#endif

//#if HAVE_MPI
//#define MY_PETSC_COMM_TYPE PETSC_COMM_WORLD
//#else
#define MY_PETSC_COMM_TYPE PETSC_COMM_SELF
//#endif



#if HAVE_PETSC
static double petscMemUsage()
{
  PetscErrorCode ierr;
  PetscLogDouble memory ;
  ierr = PetscMemoryGetCurrentUsage(&memory); CHKERRQ(ierr);
  memory /= (1024.0*1024.0);
  return memory ;
}

static inline PetscErrorCode monitor (KSP ksp, int it, PetscReal rnorm, void *mctx)
{
  std::cout << "it: " << it << "    res: " << rnorm << std::endl;
  return PetscErrorCode(0);
}
#endif

/** call PETSc solvers */
inline double solvePETSc(const std::string& parameterfile,
                         const method_t method,
                         const preconder_t preconder,
                         const int blockSize,
                         const int numberOfRows,
                         const int numberOfColumns,
                         const double* matrix,
                         const int* rows,
                         const int* columns,
                         const double* rhs,
                         double* solution,
                         std::ostream& out,
                         double& memUsage,
                         int& iterations,
                         const bool verbose = false )
{
#if HAVE_PETSC
  Vec            x,b;    /* approx solution, RHS, exact solution */
  Mat            A;      /* linear system matrix */
  KSP            ksp;    /* linear solver context */
  PC             pc;     /* preconditioner */
  PetscInt       Istart,Iend;
  PetscInt       m = numberOfRows, procs = 1,its;
  PetscErrorCode ierr;

  double reduction = 1e-8;
  readMyParameter(parameterfile, "Reduction", reduction, true );

  PetscInt restart =  10 ;
  readMyParameter(parameterfile, "GMRES_restart", restart, true );

  int maxIterFactor = 100 ;
  readMyParameter(parameterfile, "MaxIterationsFactor", maxIterFactor, true , false );

  //double relaxation = 0.9;
  //readMyParameter(parameterfile, "ILU_relaxation", relaxation, true );

  //if( preconder < amg )
  //  out << " (relax. " << relaxation << ")" << std::endl;
  //else
    out << std::endl;

  int level = preconder;
  PCType type = PCNONE ;
  if( preconder < amg )
    type = PCILU;
  else if( preconder == amg )
  {
    std::cerr << std::endl << "ERROR: PETSc-MG preconditioner not supported yet! Please choose another one. " << std::endl << std::endl;
    abort();
    type = PCMG ;
  }
  else if( preconder == jacobi )
  {
    type = PCJACOBI ;
    level = 1 ;
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
         Compute the matrix and right-hand-side vector that define
         the linear system, Ax = b.
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  {
    PetscInt* nnz = new PetscInt [ numberOfRows ];
    for( int i = 0; i<numberOfRows; ++i )
    {
      nnz[ i ] = rows[ i + 1 ] - rows[ i ];
    }

    // explicit allocation of the matrix
    if( blockSize > 1 )
    {
      PetscInt bs = blockSize;
      for( int i = 0; i<numberOfRows; ++i )
      {
        PetscInt nonBlockedVal = nnz[ i ] ;
        nnz[ i ] /= bs;
        if( (nnz[ i ] * bs) != nonBlockedVal )
        {
          std::cerr << "Blocking of row " << i << ": nnz = " << nnz[ i ] << "  normal "<< nonBlockedVal << "  bs = " << bs << std::endl;
          std::cerr << "Blocking seems to be wrong in PETSc solver call!" <<std::endl;
          abort();
        }
      }
      // setup as block matrix
      ierr = MatCreateSeqBAIJ(MY_PETSC_COMM_TYPE,bs,m,m,0,nnz,&A);CHKERRQ(ierr);
    }
    else
    {
      // normal setup
      ierr = MatCreateSeqAIJ(MY_PETSC_COMM_TYPE,m,m,0,nnz,&A);CHKERRQ(ierr);
    }
    // delete temp memory
    delete [] nnz;
  }

  // need to set default options
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);

  /*
     Currently, all PETSc parallel matrix formats are partitioned by
     contiguous chunks of rows across the processors.  Determine which
     rows of the matrix are locally owned.
  */
  ierr = MatGetOwnershipRange(A,&Istart,&Iend);CHKERRQ(ierr);

  ierr = VecCreate(MY_PETSC_COMM_TYPE,&b);CHKERRQ(ierr);
  ierr = VecSetSizes(b,PETSC_DECIDE,m * procs);CHKERRQ(ierr);
  ierr = VecSetFromOptions(b);CHKERRQ(ierr);
  ierr = VecDuplicate(b,&x);CHKERRQ(ierr);

  VecAssemblyBegin(b);
  for (PetscInt row = Istart; row < Iend; ++row )
  {
    PetscScalar v = rhs [ row ];
    VecSetValue(b, row, v, INSERT_VALUES);CHKERRQ(ierr);
  }
  VecAssemblyEnd(b);

  VecAssemblyBegin(x);
  for (PetscInt row = Istart; row < Iend; ++row )
  {
    PetscScalar v = solution[ row ];
    VecSetValue(x, row, v, INSERT_VALUES);CHKERRQ(ierr);
  }
  VecAssemblyEnd(x);

  for (PetscInt row = Istart; row < Iend; ++row )
  {
    PetscInt colEnd = rows[ row + 1 ];

    PetscInt idxm[ 1 ] = { row };

    const int colSize = colEnd - rows[ row ];
    PetscScalar v[ colSize ];
    PetscInt c[ colSize ];
    for(int col = rows[ row ], j=0; j < colSize; ++ col , ++j )
    {
      v[ j ] = matrix[ col ];
      c[ j ] = columns[ col ];
    }
    ierr = MatSetValues(A, 1, idxm, colSize, c, v ,INSERT_VALUES);CHKERRQ(ierr);
  }

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                Create the linear solver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  // stop time building preconditioning and for solving
  Timer timer ;

  /*
     Create linear solver context
  */
  ierr = KSPCreate(MY_PETSC_COMM_TYPE,&ksp);CHKERRQ(ierr);

  //  select linear solver
  if ( method == petsc_cg )
    KSPSetType(ksp, KSPCG );
  else if ( method == petsc_bicg )
    KSPSetType(ksp, KSPBCGS ); // this is the BiCG-stab version of PETSc.
  else if ( method == petsc_gmres )
  {
    KSPSetType(ksp, KSPGMRES );
    ierr = KSPGMRESSetRestart(ksp, restart); CHKERRQ(ierr);
  }
  else
  {
    assert( false );
    abort();
  }

  ierr = PCCreate(MY_PETSC_COMM_TYPE,&pc);CHKERRQ(ierr);
  ierr = PCSetType(pc, type);CHKERRQ(ierr);
  ierr = PCFactorSetLevels( pc, level + 1 );CHKERRQ(ierr);

  ierr = KSPSetPC(ksp, pc);CHKERRQ(ierr);

  /*
     Set operators. Here the matrix that defines the linear system
     also serves as the preconditioning matrix.
  */
  ierr = KSPSetOperators(ksp,A,A);CHKERRQ(ierr);

  /*
     Set linear solver defaults for this problem (optional).
     - By extracting the KSP and PC contexts from the KSP context,
       we can then directly call any KSP and PC routines to set
       various options.
  */
  PetscScalar reduc = reduction;
  PetscInt maxits = maxIterFactor * m;
  if( maxits < 0 ) maxits = PETSC_DEFAULT;
  ierr = KSPSetTolerances(ksp,reduc,1.e-50,PETSC_DEFAULT,maxits);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Solve the linear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( verbose )
  {
    ierr = KSPView(ksp,  PETSC_VIEWER_STDOUT_SELF );CHKERRQ(ierr);
    ierr = KSPMonitorSet(ksp,&monitor, PETSC_NULL, PETSC_NULL);
  }

  ierr = KSPSolve(ksp,b,x);CHKERRQ(ierr);

  ////////////////////////////////////////////////////////
  //
  //  FINSHED CALL OF SOLVER, take time
  //
  ////////////////////////////////////////////////////////

  // get time needed for solution
  const double time = timer.elapsed();

  // add memory usage
  memUsage += petscMemUsage () ;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Check solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  for (PetscInt row = Istart; row < Iend; ++row )
  {
    PetscScalar v;
    VecGetValues(x, 1, &row,  &v );CHKERRQ(ierr);
    solution[ row ] = v;
  }

  /*
     Check the error
  */
  //ierr = VecNorm(x,NORM_2,&norm);CHKERRQ(ierr);
  ierr = KSPGetIterationNumber(ksp,&its);CHKERRQ(ierr);

  out << "Number of iterations: " << its << std::endl;
  iterations = its;

  /*
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
  */
  ierr = KSPDestroy(&ksp);CHKERRQ(ierr);
  ierr = VecDestroy(&x);CHKERRQ(ierr);
  ierr = VecDestroy(&b);CHKERRQ(ierr);
  ierr = MatDestroy(&A);CHKERRQ(ierr);

  return time;
#else
  std::cerr << std::endl << "ERROR: PETSC not available, re-configure with --with-petsc=PATH_TO_PETSC!! " << std::endl;
  abort();
  return 0.0;
#endif
}
#endif // end  DUNE_PETSC_HH
