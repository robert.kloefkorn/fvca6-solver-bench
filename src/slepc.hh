#ifndef DUNE_SLEPC_HH
#define DUNE_SLEPC_HH

#include "petsc.hh"

#if HAVE_SLEPC
#include "slepceps.h"
#else
#warning "SLEPc was not found!"
#endif

inline double
calculateConditionNumber( const std::string& parameterfile,
                          const int numberOfRows,
                          const double* matrix,
                          const int* rows,
                          const int* columns,
                          std::ostream& out,
                          double& min,
                          double& max,
                          double& cond,
                          const bool verbose = false )
{
#if HAVE_SLEPC
  PetscErrorCode ierr;
  Mat         	 A;
  PetscInt    	 m = numberOfRows, Istart, Iend;
  PetscReal   	 lambda_max, lambda_min;

  EPS            eps;     /* eigenproblem solver context */
  const EPSType  type;
  PetscScalar    kr, ki;
  PetscInt       i = 0 , its, nconv;

  double tol = 1e-8;
  readMyParameter(parameterfile, "Eigensolver_tol", tol, true );

  int maxIterFactor = 100;
  readMyParameter(parameterfile, "MaxIterationsFactor", maxIterFactor, true , false );

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Generate the matrix
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  ierr = MatCreate(MY_PETSC_COMM_TYPE,&A);CHKERRQ(ierr);
  ierr = MatSetSizes(A,m,m,m,m);CHKERRQ(ierr);
  ierr = MatSetFromOptions(A);CHKERRQ(ierr);

  {
    PetscInt* nnz = new PetscInt [ numberOfRows ];
    for( int i =0; i<numberOfRows; ++i )
    {
      nnz[ i ] = rows[ i + 1 ] - rows[ i ];
    }
    // explicit allocation of the matrix
    ierr = MatSeqAIJSetPreallocation(A,0,nnz);CHKERRQ(ierr);
    delete [] nnz;
  }

  ierr = MatGetOwnershipRange(A,&Istart,&Iend);CHKERRQ(ierr);

  for (PetscInt row = Istart; row < Iend; ++row )
  {
    PetscInt colEnd = rows[ row + 1 ];

    PetscInt idxm[ 1 ] = { row };

    const int colSize = colEnd - rows[ row ];
    PetscScalar v[ colSize ];
    PetscInt c[ colSize ];
    // copy to PetscScalar
    for(int col = rows[ row ], j=0; j < colSize; ++ col , ++j )
    {
      v[ j ] = matrix[ col ];
      c[ j ] = columns[ col ];
    }
    ierr = MatSetValues(A, 1, idxm, colSize, c, v ,INSERT_VALUES);CHKERRQ(ierr);
  }

  ierr = MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY);CHKERRQ(ierr);

  /*
     Create eigensolver context
  */
  ierr = EPSCreate(MY_PETSC_COMM_TYPE,&eps);CHKERRQ(ierr);

  /*
     Set operators. In this case, it is a standard eigenvalue problem
  */
  ierr = EPSSetOperators(eps,A,PETSC_NULL);CHKERRQ(ierr);
  //ierr = EPSSetProblemType(eps,EPS_HEP);CHKERRQ(ierr);

  tol = 1e-6;
  PetscInt maxit = numberOfRows * maxIterFactor;
  if( maxit < 0 ) maxit = PETSC_DEFAULT;
  ierr = EPSSetTolerances(eps,tol,maxit);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Solve the eigensystem for largest eigenvalue
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if( verbose )
  {
    ierr = PetscPrintf(MY_PETSC_COMM_TYPE,"\n***  Start eigenvalue calculation  ***\n");CHKERRQ(ierr);
  }

  ierr = EPSSetWhichEigenpairs(eps,EPS_LARGEST_MAGNITUDE);CHKERRQ(ierr);
  ierr = EPSSolve(eps);CHKERRQ(ierr);
  ierr = EPSGetConverged(eps,&nconv);CHKERRQ(ierr);
  if( nconv > 0 )
  {
    ierr = EPSGetIterationNumber(eps, &its);CHKERRQ(ierr);
    ierr = EPSGetEigenpair(eps,i,&kr,&ki,PETSC_NULL,PETSC_NULL);CHKERRQ(ierr);
  }

  if( verbose )
  {
    ierr = EPSGetType(eps,&type);CHKERRQ(ierr);
    ierr = PetscPrintf(MY_PETSC_COMM_TYPE,"Solution method: %s\n",type);CHKERRQ(ierr);
    ierr = PetscPrintf(MY_PETSC_COMM_TYPE,"Number of iterations for largest eigenvalue:  %d\n",its);CHKERRQ(ierr);
  }

  lambda_max = kr;

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                      Solve the eigensystem for largest eigenvalue
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  ierr = EPSSetWhichEigenpairs(eps,EPS_SMALLEST_MAGNITUDE);CHKERRQ(ierr);
  ierr = EPSSolve(eps);CHKERRQ(ierr);
  if( verbose )
    ierr = PetscPrintf(MY_PETSC_COMM_TYPE,"Number of iterations for smallest eigenvalue: %d\n",its);CHKERRQ(ierr);

  ierr = EPSGetConverged(eps,&nconv);CHKERRQ(ierr);
  if( nconv > 0 )
  {
    ierr = EPSGetIterationNumber(eps, &its);CHKERRQ(ierr);
    EPSGetEigenpair(eps,i,&kr,&ki,PETSC_NULL,PETSC_NULL);//CHKERRQ(ierr);
  }
  lambda_min = kr;

  /*
     Optional: Get some information from the solver and display it
  */
  if( verbose )
  {
    ierr = EPSGetTolerances(eps,&tol,&maxit);CHKERRQ(ierr);
    ierr = PetscPrintf(MY_PETSC_COMM_TYPE,"Stopping condition: tol=%.4g, maxit=%d\n",tol,maxit);CHKERRQ(ierr);
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    Display solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

  /*
     Get number of converged approximate eigenpairs
  */
  ierr = EPSGetConverged(eps,&nconv);CHKERRQ(ierr);

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                    Display solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  min = max = cond = 0;
  if (nconv > 0 ) {
    min = lambda_min;
    max = lambda_max;
    cond = lambda_max/lambda_min;
    out << "Computed eigenvalues values: lambda_max = " << lambda_max << ", lambda_min = " << lambda_min << std::endl;
    out << "Estimated condition number:  lambda_max/lambda_min = " << lambda_max/lambda_min << std::endl;
  }

  /*
     Free work space.  All PETSc objects should be destroyed when they
     are no longer needed.
  */
  ierr = EPSDestroy(eps);CHKERRQ(ierr);
  ierr = MatDestroy(A);CHKERRQ(ierr);

  return lambda_max/lambda_min;
#else
  std::cout << "SLEPc not available, cannot compute condition number!" << std::endl;
  return 0.0;
#endif
}
#endif // DUNE_SLEPC_HH
