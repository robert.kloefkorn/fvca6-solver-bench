#ifndef ISTL_HH_INCLUDED
#define ISTL_HH_INCLUDED

//- system includes
#include <set>

//- Dune common includes
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

//- Dune istl includes
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvers.hh>

#define USE_ISTL_AMG
#ifdef USE_ISTL_AMG
#include <dune/istl/paamg/amg.hh>
#include <dune/istl/paamg/pinfo.hh>
#endif

#include "../examples/istlutility.hh"

#include "asciiparser.hh"

using namespace Dune;

template<class V>
class NoPreconditioner
  : public Preconditioner<V,V>
{
public:
    enum {
      //! \brief The category the precondtioner is part of.
      category=SolverCategory::sequential };

  typedef V X;
  typedef V Y;

  //! \copydoc Preconditioner
  virtual void pre (X& x, Y& b)
  {
  }

  //! \copydoc Preconditioner
  virtual void apply (X& v, const Y& d)
  {
    v = d ;
  }

  //! \copydoc Preconditioner
  virtual void post (X& x)
  {
  }
};

template<class Preconditioner>
class IstlPreconditionerWrapper
  : public Preconditioner
{
  Preconditioner& preCond_;
public:
  enum {
    //! \brief The category the precondtioner is part of.
    category=SolverCategory::sequential
  };

  typedef typename Preconditioner :: domain_type   X;
  typedef typename Preconditioner :: range_type    Y;

  IstlPreconditionerWrapper( Preconditioner& pre ) : preCond_( pre ) {}
  IstlPreconditionerWrapper( const IstlPreconditionerWrapper& org ) : preCond_( org.preCond_ ) {}

  //! \copydoc Preconditioner
  virtual void pre (X& x, Y& b)
  {
    preCond_.pre( x, b );
  }

  //! \copydoc Preconditioner
  virtual void apply (X& v, const Y& d)
  {
    preCond_.apply( v, d );
  }

  //! \copydoc Preconditioner
  virtual void post (X& x)
  {
    preCond_.post( x );
  }
};


template <template <class> class Solver,
          class OperatorType,
          class PreconditionerType,
          class VectorType>
inline int callSolver(OperatorType& op,
                      PreconditionerType& precond,
                      VectorType& rhs,
                      VectorType& solution,
                      const double reduction,
                      const int maxIterations,
                      const bool verbose)
{
  // scalar product
  SeqScalarProduct< VectorType > scp;

  // create solver
  typedef Solver< VectorType >  SolverType;
  SolverType solver( op, scp, precond, reduction, maxIterations, (verbose) ? 2: 0);

  // return info
  InverseOperatorResult returnInfo;

  // solve
  solver.apply( solution, rhs, returnInfo );

  return returnInfo.iterations;
}

template <template <class,class,class> class Solver,
          class OperatorType,
          class PreconditionerType,
          class VectorType>
inline int callGMRESSolver(OperatorType& op,
                           PreconditionerType& precond,
                           VectorType& rhs,
                           VectorType& solution,
                           const double reduction,
                           const int restart,
                           const int maxIterations,
                           const bool verbose)
{
  // scalar product
  SeqScalarProduct< VectorType > scp;

  // create solver
  typedef Solver< VectorType , VectorType, VectorType >  SolverType;
  SolverType solver( op, scp, precond, reduction, restart, maxIterations, (verbose) ? 2: 0);

  // return info
  InverseOperatorResult returnInfo;

  // solve
  solver.apply( solution, rhs, returnInfo );

  return returnInfo.iterations;
}

template <int blocksize>
struct ISTLData
{
  typedef FieldVector<double, blocksize> BlockType;
  typedef BlockVector< BlockType > VectorType;
  typedef FieldMatrix<double, blocksize, blocksize> BlockMatrixType;
  typedef BCRSMatrix < BlockMatrixType > MatrixType;

  MatrixType matrix;

  typedef Dune::Amg::CoarsenCriterion<
    Dune::Amg::UnSymmetricCriterion<MatrixType,Dune::Amg::FirstDiagonal> > Criterion;
  typedef SeqILUn< MatrixType, VectorType, VectorType > Smoother;
  typedef typename Dune::Amg::SmootherTraits<Smoother>::Arguments SmootherArgs;
  typedef Preconditioner< VectorType, VectorType > PreconditionerType;

  Criterion criterion;
  SmootherArgs smootherArgs;
  PreconditionerType* precond;

  ISTLData(int r, int c, int coarsenTarget) :
    matrix(r,c, MatrixType :: row_wise),
    criterion(15, coarsenTarget),
    smootherArgs(),
    precond(0)
  {}
  ~ISTLData() { delete precond; }
};

template <int blocksize>
inline std::pair<double,int>
solveISTL(const std::string& parameterfile,
          const method_t method,
          const preconder_t preconder,
          const int numberOfRows,
          const int numberOfColumns,
          const double* matrixEntries,
          const int* rows,
          const int* columns,
          const double* rhsEntries,
          double* solEntries,
          std::ostream& out,
          ISTLData<blocksize> *&data,
          const bool verbose = true)
{
  // type of little blocks
  typedef FieldMatrix<double, blocksize, blocksize> BlockMatrixType;
  typedef FieldVector<double, blocksize> BlockType;

  // type of matrix
  typedef BCRSMatrix < BlockMatrixType > MatrixType;

  // adjust number of rows by blocksize
  const int realRows = (numberOfRows)    / blocksize;
  const int realCols = (numberOfColumns) / blocksize;

  // make sure sizes fit
  assert( realRows * blocksize == numberOfRows );
  assert( realCols * blocksize == numberOfColumns );

  int coarsenTarget=1200;
  bool first = !data;
  if (first)
  {
    data = new ISTLData<blocksize>( realRows, realCols, coarsenTarget );
    copyToBlockMatrix( numberOfRows, numberOfColumns,
                       matrixEntries, rows, columns, data->matrix );
  }

  MatrixType &matrix = data->matrix;

  if( verbose )
    std::cout << "ISTL-Matrix assembled: rows = " << matrix.N() << " cols = " << matrix.M() << " containing block of size " << blocksize << std::endl;

  typedef BlockVector< BlockType > VectorType;

  VectorType rhs( realRows );
  copyToBlockVector( rhsEntries, rhs );

  VectorType solution( realCols );
  copyToBlockVector( solEntries, solution );

  double reduction = 1e-8;
  readMyParameter(parameterfile, "Reduction", reduction, true );

  double relaxation = 1.0;
  readMyParameter(parameterfile, "ILU_relaxation", relaxation, true );

  int restart = 10 ;
  readMyParameter(parameterfile, "GMRES_restart", restart, true );

  int maxIterFactor = 100 ;
  readMyParameter(parameterfile, "MaxIterationsFactor", maxIterFactor, true , false );

  int preIteration = 1;
  readMyParameter(parameterfile, "PreIteration", preIteration, true , false );

  if( preconder >= 0 && preconder < amg )
    out << " (relax. " << relaxation << ")" << std::endl;
  else
    out << std::endl;

  out << "Reduction to achieve: " << reduction << std::endl;

  typedef Dune::Amg::CoarsenCriterion<
    Dune::Amg::UnSymmetricCriterion<MatrixType,Dune::Amg::FirstDiagonal> > Criterion;

  // preconditioner
  typedef SeqILUn< MatrixType, VectorType, VectorType > Smoother;
  typedef typename Dune::Amg::SmootherTraits<Smoother>::Arguments SmootherArgs;
  typedef Preconditioner< VectorType, VectorType > PreconditionerType;

  if (first)
  {
    SmootherArgs &smootherArgs = data->smootherArgs;
    smootherArgs.iterations = 2;
    smootherArgs.relaxationFactor = relaxation ;
    Criterion &criterion = data->criterion;
    criterion.setDefaultValuesIsotropic(2);
    criterion.setAlpha(.67);
    criterion.setBeta(1.0e-8);
    criterion.setMaxLevel(10);
    data->precond = 0;
  }

  PreconditionerType* &precond = data->precond;
  // matrix adapter
  typedef MatrixAdapter< MatrixType, VectorType, VectorType > OperatorType;
  OperatorType op( matrix );

  ////////////////////////////////////////////////////////
  //
  //  Build preconditioner and CALL OF SOLVER, take time now
  //
  ////////////////////////////////////////////////////////
  Timer timer ;

  if (precond == 0)
  {
    if( preconder == none )
      precond = new NoPreconditioner< VectorType > ();
    else if ( preconder == ilu_0 )
      precond = new SeqILU0< MatrixType, VectorType, VectorType > ( matrix, relaxation );
    else if ( preconder >= ilu_1 && preconder < amg )
      precond = new SeqILUn< MatrixType, VectorType, VectorType > ( matrix, preconder, relaxation );
#ifdef USE_ISTL_AMG
    else if ( preconder == amg )
    {
      typedef Dune::Amg::AMG<OperatorType,VectorType,Smoother> AMG;
      precond = new AMG(op, data->criterion, data->smootherArgs, 1, 1, 1, false);
    }
#endif
    else if ( preconder == jacobi )
    {
      precond = new SeqJac< MatrixType, VectorType, VectorType >(matrix, preIteration, relaxation);
    }
    else
    {
      std::cerr << "Preconditioner " << preconder << " is not supported! " << std::endl;
      abort();
    }
  }

  assert( precond );

  IstlPreconditionerWrapper< PreconditionerType > preConder( *precond );

  int iterations = 0;
  // get current time

  int maxIter = 0;
  readMyParameter(parameterfile, "maxIterations", maxIter, true , false );
  if (maxIter == 0)
    maxIter = realRows * maxIterFactor;
  if ( method < istl_bicg )
  {
    iterations =  callSolver< CGSolver >
      ( op, preConder, rhs, solution, std::abs( reduction ), maxIter, verbose);
  }
  else if( method == istl_bicg )
  {
    iterations = callSolver< BiCGSTABSolver >
      ( op, preConder, rhs, solution, std::abs( reduction ), maxIter, verbose);
  }
  else
  {
    iterations = callGMRESSolver< RestartedGMResSolver >
      ( op, preConder, rhs, solution, std::abs( reduction ), restart, maxIter, verbose);
  }

  out << "Number of iterations: " << iterations << std::endl;

  ////////////////////////////////////////////////////////
  //
  //  FINSHED CALL OF SOLVER, take time
  //
  ////////////////////////////////////////////////////////

  // get time needed for solution
  double time = timer.elapsed();

  // store solution
  copyToVector( solution, solEntries );

  return std::make_pair(time, iterations);
}
#endif
