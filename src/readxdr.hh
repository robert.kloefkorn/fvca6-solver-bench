#ifndef READXDR_HH_INCLUDED
#define READXDR_HH_INCLUDED

// include matrix structure
#include "mymatrix.hh"

// inlcude xdr read-write methods
extern "C" {
  #include "../examples/writexdr.h"
}

template <int blocksize>
MatrixIF * createCRSMatrix(const char* filename )
{
  XDR xdrs;
  FILE* file = fopen(filename, "rb");

  /* check file */
  if( ! file )
  {
    fprintf(stderr,"Couldn't open file `%s' for reading! \n", filename);
    exit( EXIT_FAILURE );
  }

  /* create XDR stream for reading */
  xdrstdio_create( &xdrs, file, XDR_DECODE);

  setXDRReadPointer( &xdrs );

  int storedBlockSize, numberNonZeros, numberOfRows, numberOfColumns;
  int testNumber = -1;
  char* nameAndScheme = 0;
  char* mesh = 0;

  /* read dimensions from file */
  int freeMem = readWriteHeader( readwrite_int_xdr,
                                 readwrite_double_xdr,
                                 readwrite_string_xdr,
                                 & nameAndScheme,
                                 & testNumber,
                                 & mesh,
                                 & storedBlockSize,
                                 & numberOfRows,
                                 & numberOfColumns,
                                 & numberNonZeros );

  MatrixIF* matrix = 0;
  std::cout << "Name and scheme: " << nameAndScheme << std::endl;
  std::cout << "Mesh: " << mesh << std::endl;

  if( blocksize != storedBlockSize )
  {
    std::cerr << "ERROR: wrong block size - expected " << blocksize << ", read " << storedBlockSize << ", thus defaulting to 1" << std::endl;

    typedef CRSMatrix<1> MatrixType;
    // create matrix
    matrix = new MatrixType( nameAndScheme,
                             testNumber,
                             mesh,
                             numberOfRows,
                             numberOfColumns,
                             numberNonZeros,
                             filename);
  }
  else
  {
    typedef CRSMatrix<blocksize> MatrixType;
    // create matrix
    matrix = new MatrixType( nameAndScheme,
                             testNumber,
                             mesh,
                             numberOfRows,
                             numberOfColumns,
                             numberNonZeros,
                             filename);
  }

  /* free memory allocalted by readWriteHeader */
  if( freeMem )
  {
    free( nameAndScheme );
    free( mesh );
  }

  // check matrix
  assert( matrix );

  // read matrix entries
  matrix->readXDR( readwrite_int_xdr, readwrite_double_xdr );

  /* destroy XDR stream */
  xdr_destroy( &xdrs );

  /* close file */
  fclose( file );

  return matrix;
}
#endif
