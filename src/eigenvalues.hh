#ifndef SOLVERBENCH_EIGENVALUES_HH
#define SOLVERBENCH_EIGENVALUES_HH

#include <iostream>
#include <cmath>
#include <cassert>

#if HAVE_LAPACK
// dsyev declaration (in liblapack)
extern "C" {

/*
    *
    **  purpose
    **  =======
    **
    **  xsyev computes all eigenvalues and, optionally, eigenvectors of a
    **  BASE DATA TYPE symmetric matrix a.
    **
    **  arguments
    **  =========
    **
    **  jobz    (input) char
    **          = 'n':  compute eigenvalues only;
    **          = 'v':  compute eigenvalues and eigenvectors.
    **
    **  uplo    (input) char
    **          = 'u':  upper triangle of a is stored;
    **          = 'l':  lower triangle of a is stored.
    **
    **  n       (input) long int
    **          the order of the matrix a.  n >= 0.
    **
    **  a       (input/output) BASE DATA TYPE array, dimension (lda, n)
    **          on entry, the symmetric matrix a.  if uplo = 'u', the
    **          leading n-by-n upper triangular part of a contains the
    **          upper triangular part of the matrix a.  if uplo = 'l',
    **          the leading n-by-n lower triangular part of a contains
    **          the lower triangular part of the matrix a.
    **          on exit, if jobz = 'v', then if info = 0, a contains the
    **          orthonormal eigenvectors of the matrix a.
    **          if jobz = 'n', then on exit the lower triangle (if uplo='l')
    **          or the upper triangle (if uplo='u') of a, including the
    **          diagonal, is destroyed.
    **
    **  lda     (input) long int
    **          the leading dimension of the array a.  lda >= max(1,n).
    **
    **  w       (output) BASE DATA TYPE array, dimension (n)
    **          if info = 0, the eigenvalues in ascending order.
    **
    **
    **
    **  info    (output) long int
    **          = 0:  successful exit
    **          < 0:  if info = -i, the i-th argument had an illegal value
    **          > 0:  if info = i, the algorithm failed to converge; i
    **                off-diagonal elements of an intermediate tridiagonal
    **                form did not converge to zero.
    **
**/
extern void dsyev_(const char* jobz, const char* uplo, const long
                   int* n, double* a, const long int* lda, double* w,
                   double* work, const long int* lwork, long int* info);
/*
    *
    **  purpose
    **  =======
    **
    **  xgeev computes for an n-by-n DATA TYPE nonsymmetric matrix a, the
    **  eigenvalues and, optionally, the left and/or right eigenvectors.
    **
    **  the right eigenvector v(j) of a satisfies
    **                   a * v(j) = lambda(j) * v(j)
    **  where lambda(j) is its eigenvalue.
    **  the left eigenvector u(j) of a satisfies
    **                u(j)**h * a = lambda(j) * u(j)**h
    **  where u(j)**h denotes the conjugate transpose of u(j).
    **
    **  the computed eigenvectors are normalized to have euclidean norm
    **  equal to 1 and largest component BASE DATA TYPE.
    **
    **  arguments
    **  =========
    **
    **  jobvl   (input) char
    **          = 'n': left eigenvectors of a are not computed;
    **          = 'v': left eigenvectors of are computed.
    **
    **  jobvr   (input) char
    **          = 'n': right eigenvectors of a are not computed;
    **          = 'v': right eigenvectors of a are computed.
    **
    **  n       (input) long int
    **          the order of the matrix a. n >= 0.
    **
    **  a       (input/output) DATA TYPE array, dimension (lda,n)
    **          on entry, the n-by-n matrix a.
    **          on exit, a has been overwritten.
    **
    **  lda     (input) long int
    **          the leading dimension of the array a.  lda >= max(1,n).
    *  WR      (output) DOUBLE PRECISION array, dimension (N)
    *  WI      (output) DOUBLE PRECISION array, dimension (N)
    *          WR and WI contain the real and imaginary parts,
    *          respectively, of the computed eigenvalues.  Complex
    *          conjugate pairs of eigenvalues appear consecutively
    *          with the eigenvalue having the positive imaginary part
    *          first.

    **
    **  vl      (output) DATA TYPE array, dimension (ldvl,n)
    **          if jobvl = 'v', the left eigenvectors u(j) are stored one
    **          after another in the columns of vl, in the same order
    **          as their eigenvalues.
    **          if jobvl = 'n', vl is not referenced.
    **          u(j) = vl(:,j), the j-th column of vl.
    **
    **  ldvl    (input) long int
    **          the leading dimension of the array vl.  ldvl >= 1; if
    **          jobvl = 'v', ldvl >= n.
    **
    **  vr      (output) DATA TYPE array, dimension (ldvr,n)
    **          if jobvr = 'v', the right eigenvectors v(j) are stored one
    **          after another in the columns of vr, in the same order
    **          as their eigenvalues.
    **          if jobvr = 'n', vr is not referenced.
    **          v(j) = vr(:,j), the j-th column of vr.
    **
    **  ldvr    (input) long int
    **          the leading dimension of the array vr.  ldvr >= 1; if
    **          jobvr = 'v', ldvr >= n.
    **
    **
    **
    **
    **  info    (output) long int
    **          = 0:  successful exit
    **          < 0:  if info = -i, the i-th argument had an illegal value.
    **          > 0:  if info = i, the qr algorithm failed to compute all the
    **                eigenvalues, and no eigenvectors have been computed;
    **                elements and i+1:n of w contain eigenvalues which have
    **                converged.
    **
**/
   extern void dgeev_(
        const char* jobvl,
        const char* jobvr,
        const long int* n,
        double* a,
        const long int* lda,
        double* wr,
        double* wi,
        const double* vl,
        const long int* ldvl,
        const double* vr,
        const long int* ldvr,
        double* work, const long int* lwork,
        long int* info);
/**
    *
    **  purpose
    **  =======
    **
    **  xgetrf computes an lu factorization of a general m-by-n matrix a
    **  using partial pivoting with row interchanges.
    **
    **  the factorization has the form
    **     a = p * l * u
    **  where p is a permutation matrix, l is lower triangular with unit
    **  diagonal elements (lower trapezoidal if m > n), and u is upper
    **  triangular (upper trapezoidal if m < n).
    **
    **  this is the right-looking level 3 blas version of the algorithm.
    **
    **  arguments
    **  =========
    **
    **  m       (input) long int
    **          the number of rows of the matrix a.  m >= 0.
    **
    **  n       (input) long int
    **          the number of columns of the matrix a.  n >= 0.
    **
    **  a       (input/output) DATA TYPE array, dimension (lda,n)
    **          on entry, the m-by-n matrix to be factored.
    **          on exit, the factors l and u from the factorization
    **          a = p*l*u; the unit diagonal elements of l are not stored.
    **
    **  lda     (input) long int
    **          the leading dimension of the array a.  lda >= max(1,m).
    **
    **  ipiv    (output) long int array, dimension (min(m,n))
    **          the pivot indices; for 1 <= i <= min(m,n), row i of the
    **          matrix was interchanged with row ipiv(i).
    **
    **  info    (output) long int
    **          = 0:  successful exit
    **          < 0:  if info = -i, the i-th argument had an illegal value
    **          > 0:  if info = i, u(i,i) is exactly zero. the factorization
    **                has been completed, but the factor u is exactly
    **                singular, and division by zero will occur if it is used
    **                to solve a system of equations.
    **
**/
   extern void dgetrf_(
        const long int* n,
        const long int* m,
        double* a,
        const long int* lda,
        const long int* ipvp,
        long int* info);


/**
    **  purpose
    **  =======
    **
    **  xgecon estimates the reciprocal of the condition number of a general
    **  DATA TYPE matrix a, in either the 1-norm or the infinity-norm, using
    **  the lu factorization computed by cgetrf.
    **
    **  an estimate is obtained for norm(inv(a)), and the reciprocal of the
    **  condition number is computed as
    **     rcond = 1 / ( norm(a) * norm(inv(a)) ).
    **
    **  arguments
    **  =========
    **
    **  norm    (input) char
    **          specifies whether the 1-norm condition number or the
    **          infinity-norm condition number is required:
    **          = '1' or 'o':  1-norm;
    **          = 'i':         infinity-norm.
    **
    **  n       (input) long int
    **          the order of the matrix a.  n >= 0.
    **
    **  a       (input) DATA TYPE array, dimension (lda,n)
    **          the factors l and u from the factorization a = p*l*u
    **          as computed by cgetrf.
    **
    **  lda     (input) long int
    **          the leading dimension of the array a.  lda >= max(1,n).
    **
    **  anorm   (input) BASE DATA TYPE
    **          if norm = '1' or 'o', the 1-norm of the original matrix a.
    **          if norm = 'i', the infinity-norm of the original matrix a.
    **
    **  rcond   (output) BASE DATA TYPE
    **          the reciprocal of the condition number of the matrix a,
    **          computed as rcond = 1/(norm(a) * norm(inv(a))).
    **
    **
    **
    **  info    (output) long int
    **          = 0:  successful exit
    **          < 0:  if info = -i, the i-th argument had an illegal value
    **
**/
   extern void dgecon_(
        const char* norm,
        const long int* n,
        double* a,
        const long int* lda,
        const long int* ipvp,
        long int* info);


/////////////////////////////////////////////////////////////////////////////////
} // end extern C
#endif

/** \brief calculates the condition number of a matrix using LAPACK
    \param[in]  matrix matrix condition number is calculated for
*/
template <class MatrixType>
static double calculateConditionNumber(const MatrixType& matrix)
{
#if HAVE_LAPACK
  try {
    assert( matrix.rows() == matrix.cols() );
    const long int N = matrix.rows();
    assert( matrix.rows() == matrix.cols() );
    const char jobz = 'n'; // only calculate eigen values
    const char uplo = 'u'; // use upper triangular matrix
    const int dim = N;

    // length of matrix vector
    const long int w = N * N ;

    // matrix to put into dsyev
    double* matrixVector = new double [dim * dim];
    double* eigenvalues = new double [dim];

    const bool symetric = matrix.denseMatrix( matrixVector );

    // working memory
    double* workSpace = new double [dim * dim];

    // return value information
    long int info = 0;

    /*
    {
      long int* ipvp = new long int[ N ];
      dgetrf_(&N, &N, &matrixVector[0], &N, &ipvp[0], &info);
      std::cout << "Calculated LU \n";
    }
    */

    std::string sym( (symetric) ? "symetric" : "non-symetric" );
    std::cout << "Matrix is " << sym << std::endl;

    if( symetric )
    {
      // call LAPACK dsyev
      dsyev_(&jobz, &uplo, &N, &matrixVector[0], &N,
             &eigenvalues[0], &workSpace[0], &w, &info);
    }
    else
    {
      const char jobsvl = 'n';
      const char jobsvr = 'n';

      double *VL = new double [ N ];
      double *VR = new double [ N ];
      double *imEv = new double [ N ];

      dgeev_( &jobsvl, &jobsvr, &N, &matrixVector[0], &N, &eigenvalues[0], &imEv[0],
              &VL[0], &N, &VR[0], &N, &workSpace[0], &w, &info );

      delete [] VL;
      delete [] VR;
      delete [] imEv;
    }

    delete [] workSpace;
    delete [] matrixVector;

    std::cout << "Eigenvalues: " << std::endl;
    bool negative = false ;
    double minE=1e10;
    double maxE=-1e10;
    for(int i=0; i<dim; ++i)
    {
      if( eigenvalues[ i ] < 0 ) negative = true ;
      minE = std::min( minE, eigenvalues[ i ] );
      maxE = std::max( maxE, eigenvalues[ i ] );
    }

    std::cout << "Range: [" << minE << "," << maxE << "]" << std::endl;

    // assert( ! negative );

    delete [] eigenvalues;

    if( info != 0 )
    {
      abort();
    }
    return maxE / minE ;
  }
  catch ( std::bad_alloc )
  {
    std::cerr << "Cannot allocate for dim = " << matrix.rows() << std::endl;
    std::cerr << "Condition number calculation: out of memory!" << std::endl;
  }
  return 1e300;
#else
#error
  return 0.0;
#endif
}

#endif
