#undef ENABLE_MPI
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>  // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include "mymatrix.hh"
#include "readxdr.hh"

extern "C" {
  extern int readBlockSize(const char* filename);
}

using namespace Dune;

int main(int argc, char** argv)
{
  try{
    // initialize MPI (if enabled, see DUNE docu)
    MPIHelper::instance(argc, argv);

#if HAVE_PETSC
    static char help[] = "Petsc-Slepc init";
#endif
#if HAVE_SLEPC
    SlepcInitialize(&argc,&argv,(char*)0,help);
#elif HAVE_PETSC
    PetscInitialize(&argc,&argv,(char *)0,help);
#endif

    const char* datafilename  = "matrixfile.xdr";
    const char* parametername = "parameter";

    if( argc >= 2 )
    {
      // get datafileanme
      datafilename = argv[1];
      if( argc >= 3 )
      {
        // get parameter filename
        parametername = argv[ 2 ];
      }
    }
    else
    {
      std::cerr << "Usage: " << argv[0] << " <datafile.xdr> <parameter file>" << std::endl;
    }

    const int blockSize = readBlockSize( datafilename );

    std::cout << "Reading parameters: " << std::endl;
    std::cout << "Read block size from data file: block size = " << blockSize << std::endl;

    std::cout << "Reading parameters from file `" << parametername << "':" << std::endl;

    int useBlocking = 1;
    readMyParameter( parametername, "UseBlocking", useBlocking, true, false );

    // create structures holding matrix information
    // see readxdr.hh
    MatrixIF* matrix = 0;

    if( blockSize == 1 || ! useBlocking )
    {
      matrix = createCRSMatrix< 1 >( datafilename );
    }
    else if( blockSize == 2 )
    {
      matrix = createCRSMatrix< 2 >( datafilename );
    }
    else if( blockSize == 3 )
    {
      matrix = createCRSMatrix< 3 >( datafilename );
    }
    else if( blockSize == 4 )
    {
      matrix = createCRSMatrix< 4 >( datafilename );
    }
    else if( blockSize == 5 )
    {
      matrix = createCRSMatrix< 5 >( datafilename );
    }
    else if( blockSize == 6 )
    {
      matrix = createCRSMatrix< 6 >( datafilename );
    }
    else if( blockSize == 8 )
    {
      matrix = createCRSMatrix< 8 >( datafilename );
    }
    else if( blockSize == 10 )
    {
      matrix = createCRSMatrix< 10 >( datafilename );
    }
    else if( blockSize == 15 )
    {
      matrix = createCRSMatrix< 15 >( datafilename );
    }
    else if( blockSize == 20 )
    {
      matrix = createCRSMatrix< 20 >( datafilename );
    }
    else if( blockSize == 27 )
    {
      matrix = createCRSMatrix< 27 >( datafilename );
    }
    else if( blockSize == 35 )
    {
      matrix = createCRSMatrix< 35 >( datafilename );
    }
    else if( blockSize == 64 )
    {
      matrix = createCRSMatrix< 64 >( datafilename );
    }
    else if( blockSize == 125 )
    {
      matrix = createCRSMatrix< 125 >( datafilename );
    }
    else
    {
      std::cerr << "ERROR: block size " << blockSize << " currently not implemented, adjust the code and re-compile! " << std::endl;
      return 1;
    }

    // call solver
    matrix->solve( parametername );

    delete matrix;

#if HAVE_SLEPC
    {
      PetscErrorCode ierr = 0;
      ierr = SlepcFinalize();CHKERRQ(ierr);
    }
#elif HAVE_PETSC
    {
      PetscErrorCode ierr = 0;
      ierr = PetscFinalize();CHKERRQ(ierr);
    }
#endif
    return 0;
  }
  catch (Dune::Exception &e){
    std::cerr << "Dune reported error: " << e << std::endl;
  }
  catch (...){
    std::cerr << "Unknown exception thrown!" << std::endl;
  }
}
