#ifndef OEMSOLVER_INCLUDED
#define OEMSOLVER_INCLUDED

#include <cmath>
// timer class using get_rusage
#include <dune/common/timer.hh>
#include <dune/common/version.hh>

#if DUNE_VERSION_NEWER(DUNE_COMMON,2,3)
#include <dune/common/parallel/collectivecommunication.hh>
#else
#include <dune/common/collectivecommunication.hh>
#endif

#if HAVE_MPI
#include <dune/common/mpicollectivecommunication.hh>
#endif
#if HAVE_BLAS
#include "oem/oemsolver.hh"
#endif

/* \brief OEM solver solve Ax = b
   \param n        number of unknowns
   \param matrix   system matrix
   \param rows     positions of rows in vector matrix
   \param columns  real column numbers
   \param exactSolution solution calculated before and stored in file
   \param solution solution to be calculated here
   \param verbose  true for verbose mode

   \return CPU time needed for solution
*/
template <class Matrix>
double solveOEM(const int n,
                const Matrix& matrix,
                const double* rhs,
                double* solution,
                const bool gmres,
                const bool verbose = true )
{
#if HAVE_BLAS
#if HAVE_MPI
  Dune::CollectiveCommunication< MPI_Comm > comm(
      MPIHelper::getCommunicator());
#else
  Dune::CollectiveCommunication< Matrix > comm;
#endif
  Dune::Timer overall;

  const int innerCycle = 10;
  if( gmres )
    OEMSolver::gmres(comm,innerCycle,n,matrix,rhs,solution,1e-17,verbose);
  else
    OEMSolver::cghs(comm,n,matrix,rhs,solution,1e-17,verbose);

  const double cpuTime = overall.elapsed();
  const char* solver = ( gmres ) ? "OEM-GMRES" : "OEM-CG";
  std::cout << "Overall time for " << solver << ": " << cpuTime << std::endl;

  // return time needed for solution
  return cpuTime;
#else
  std::cerr << "ERROR: BLAS lib not found! " << std::endl;
  return 0.0;
#endif
}

#endif
