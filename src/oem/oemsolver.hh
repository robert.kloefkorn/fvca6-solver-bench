#ifndef DUNE_FEM_OEMSOLVER_HH
#define DUNE_FEM_OEMSOLVER_HH

//- system includes
#include <limits>
#include <utility>

// include BLAS  implementation
#include "cblas.h"

namespace Dune
{

  namespace Fem
  {

  /** \class   OEMMatrix
   *  \ingroup OEMSolver
   *  \brief   interface for matrices to be used with OEM sovlers
   */
  struct OEMMatrix
  {
    virtual ~OEMMatrix () {}

    /** \brief evaluate matrix vector multiplication
     *
     *  \param[in]   u  vector to multiply the matrix with
     *  \param[out]  w  vector to store the result in
     */
    virtual void multOEM ( const double *u, double *w ) const = 0;

    /** \brief evaluate scalar product
     *
     *  \param[in]   u  first argument of scalar product
     *  \param[in]   v  second argument of scalar product
     */
    virtual double ddotOEM ( const double *u, const double *v ) const = 0;
  };

  }

}



namespace OEMSolver
{

// use cblas implementations
using namespace DuneCBlas;

using DuneCBlas :: daxpy;
using DuneCBlas :: dcopy;
using DuneCBlas :: ddot;
using DuneCBlas :: dnrm2;
using DuneCBlas :: dscal;

//! this method is called from all solvers and is only a wrapper
//! this method is mainly from SparseRowMatrix
template <class MatrixImp, class VectorType>
void mult(const MatrixImp & m, const VectorType * x, VectorType * ret)
{
  // call multOEM of the matrix
  m.multOEM(x,ret);
}

//! mult method when given pre conditioning matrix
template <class Matrix , class PC_Matrix , bool >
struct Mult
{
  static inline double ddot( const Matrix& A,
                             const double *x,
                             const double *y)
  {
    return A.ddotOEM(x,y);
  }

  typedef bool mult_t(const Matrix &A,
                      const PC_Matrix & C,
                      const double *arg,
                      double *dest ,
                      double * tmp);

  static void back_solve(const int size,
        const PC_Matrix & C, double* solution, double* tmp)
  {
    assert( tmp );
    if( C.rightPrecondition() )
    {
      C.precondition(solution,tmp);
      // copy modified solution
      std::memcpy(solution,tmp, size * sizeof(double));
    }
  }

  static bool mult_pc (const Matrix &A, const PC_Matrix & C,
        const double *arg, double *dest , double * tmp)
  {
    assert( tmp );

    bool rightPreCon = C.rightPrecondition();
    // check type of preconditioning
    if( rightPreCon )
    {
      // call precondition of Matrix PC
      C.precondition(arg,tmp);

      // call mult of Matrix A
      mult(A,tmp,dest);
    }
    else
    {
      // call mult of Matrix A
      mult(A,arg,tmp);

      // call precondition of Matrix PC
      C.precondition(tmp,dest);
    }
    return rightPreCon ;
  }
};

//! mult method when no pre conditioning matrix
template <class Matrix>
struct Mult<Matrix,Matrix,false>
{
  static inline double ddot( const Matrix& A,
                             const double *x,
                             const double *y)
  {
    return A.ddotOEM(x,y);
  }

  typedef bool mult_t(const Matrix &A,
                      const Matrix &C,
                      const double *arg,
                      double *dest ,
                      double * tmp);

  static void back_solve(const int size,
        const Matrix & C, double* solution, double* tmp)
  {
    // do nothing here
  }

  static bool mult_pc(const Matrix &A, const Matrix & C, const double *arg ,
                      double *dest , double * tmp)
  {
    // tmp has to be 0
    assert( tmp == 0 );
    // C is just a fake
    assert( &A == &C );

    // call mult of Matrix A
    mult(A,arg,dest);

    return true;
  }
};

#include "cghs.h"
#include "gmres.h"

} // end namespace OEMSolver
#endif
