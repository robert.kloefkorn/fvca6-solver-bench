#ifndef UMFPACK_INCLUDED
#define UMFPACK_INCLUDED

#include <cmath>
#include <iostream>
// timer class using get_rusage
#include <dune/common/timer.hh>

#if HAVE_UMFPACK
#include <umfpack.h>
#else
#warning "UMFPACK was not found!"
#endif

/* \brief class UMFPACK solver and solve Ax = b
   \param matrix   system matrix
   \param rows     positions of rows in vector matrix
   \param columns  real column numbers
   \param
   \param x solution

   \return CPU time needed for solution
*/
inline double solveUMF(const int numberOfRows,
                       const int numberOfColumns,
                       double* matrix,
                       int* rows,
                       int* columns,
                       double* rhs,
                       double* solution,
                       double& memUsage,
                       const bool verbose = false )
{
#ifdef ENABLE_UMFPACK
  double *null = (double *) NULL ;
  void *Symbolic, *Numeric;

  // assume matrix is n times n matrix
  const int n = numberOfRows;
  const int m = numberOfColumns;

  if( verbose )
  {
    std::cout << "nR = " << numberOfRows << " nC = " << numberOfColumns << " nZ = " << rows[ n ] << std::endl;
    std::cout << "Rows offset is " << rows[0] << std::endl;
  }

  // info array for umfpack
  double* Info = new double [UMFPACK_INFO];

  // take current time
  Dune :: Timer timer;

  // call solver
  umfpack_di_symbolic (n, m, rows, columns, matrix, &Symbolic, null, Info) ;
  umfpack_di_numeric (rows, columns, matrix, Symbolic, &Numeric, null, Info) ;

  // UMFPACK expects the matrix to be stored column wise, so we just solve A^T x = b instead
  umfpack_di_solve (UMFPACK_At, rows, columns, matrix, solution, rhs, Numeric, null, Info) ;

  // get cpu needed for solution
  const double cpuTime = timer.elapsed();

  // get umfpacks peak memory estimate
  memUsage = Info[ UMFPACK_PEAK_MEMORY_ESTIMATE ]/1024.0/1024.0;

  umfpack_di_free_numeric (&Numeric) ;
  umfpack_di_free_symbolic (&Symbolic) ;

  // free temp mem
  delete [] Info ;

  //std::cout << "Overall time for UMFPACK solve: " << timer.elapsed() << std::endl;
  // return time needed for solution
  return cpuTime;
#else
  std::cerr << std::endl << "ERROR: UMFPack not available, re-configure with --with-umfpack=PATH_TO_UMFPACK!! " << std::endl;
  abort();
  return 0.0;
#endif
}
#endif
