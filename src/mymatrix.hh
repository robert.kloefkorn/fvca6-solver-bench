#ifndef MYMATRIX_HH_INCLUDED
#define MYMATRIX_HH_INCLUDED

#include <cstdlib>
#include <cassert>
#include <cmath>
#include <map>
#include <utility>

#include <string>
#include <sstream>

#include <sys/time.h>
#include <sys/resource.h>

// method names
static const std::string solverName [] = { "UMFPACK di_solve",
                                           "ISTL-CG",
                                           "ISTL-BiCGstab",
                                           "ISTL-GMRES",
                                           "PETSc-CG",
                                           "PETSc-BiCGstab",
                                           "PETSc-GMRES",
                                           "OEM-CG",
                                           "OEM-GMRES" };

// method names
static const std::string preconderName [] = { "ILU (0)",
                                              "ILU (1)",
                                              "ILU (2)",
                                              "ILU (3)",
                                              "ILU (4)",
                                              "AMG( MG )",
                                              "JACOBI",
                                              "NONE" };

enum method_t { umfpack          = 0,
                istl_cg          = 1,
                istl_bicg        = 2,
                istl_gmres       = 3,
                petsc_cg         = 4,
                petsc_bicg       = 5,
                petsc_gmres      = 6,
                oem_cg = petsc_gmres + 1,
                oem_gmres = oem_cg + 1  };

enum preconder_t { ilu_0 = 0,
                   ilu_1 = 1,
                   ilu_2 = 2,
                   ilu_3 = 3,
                   ilu_4 = 4,
                   amg = 5,
                   jacobi = 6,
                   none = 7 };

#include "umfpack.hh"
#include "istl.hh"
#include "petsc.hh"
#include "slepc.hh"
#include "oem.hh"
#include "eigenvalues.hh"
#include "asciiparser.hh"

inline double getAllocMem()
{
#if HAVE_PETSC
  return petscMemUsage(); //memSizeAlloc/1024.0/1024.0;
#else
  return 0.0;
#endif
}

extern "C" {
#include "../examples/writexdr.h"
  extern FILE* filePtr;
  extern int writeMode;
}

class MatrixIF
{
protected:
  MatrixIF () {}
public:
  //! destructor
  virtual ~MatrixIF () {}

  //! solve method call the appropriate solver
  virtual double solve(const std::string& ) = 0;

  //! read matrix from XDR stream
  virtual void readXDR(rw_int_t* rw_int, rw_double_t* rw_double) = 0;
};

template <int blockSize>
class CRSMatrix : public MatrixIF
                , public Fem :: OEMMatrix
{
  // don't allow copying since this is not implemented corretly
  CRSMatrix( const CRSMatrix& other );
protected:
  double* matrix_;
  int* rows_;
  int* columns_;

  double* rhs_;
  double* solution_;
  double* guess_;

  const std::string nameAndScheme_ ;
  const int testNumber_;
  const std::string mesh_ ;

  int nRows_;
  int nColumns_;
  int nonZeros_;
  double matrixMem_ ;
  double memUsage_ ;
  const int coloffset_;
  std::string filename_;

  mutable std::vector<double> numbers_;

  bool verbose_ ;
  bool first_;
  ISTLData<blockSize> *istlData_;

  void allocateMemory()
  {
    matrixMem_ = getAllocMem();

    matrix_   = new double [ nonZeros_ ];
    rows_     = new int [ nRows_ + 1 ];
    memset( rows_ , 0 , nRows_*sizeof(int) );
    columns_  = new int [ nonZeros_ ];

    rhs_      = new double [ nRows_ ];
    solution_ = new double [ nColumns_ ];
    guess_    = new double [ nColumns_ ];

    clearVector( guess_ );
    clearVector( solution_ );

    // store matrix memory
    matrixMem_ = getAllocMem() - matrixMem_;
  }

  double computeMemoryUsage(const double matrixMem, const double factor ) const
  {
    const double memUse = getMemoryUsage();
    if( memUse > 0 )
      return ( factor < 0 ) ? memUse + factor * matrixMem : memUse;
    else
      return 0;
  }

  //! get memory in MB
  double getMemoryUsage() const
  {
    struct rusage info;
    getrusage( RUSAGE_SELF, &info );
    return (info.ru_maxrss / 1024.0);
  }

  void clearVector(double * vec)
  {
    // initialize guess with 0
    memset( vec, 0 , nColumns_*sizeof(double) );
  }
public:
  //! constructor taking number of rows and columns
  CRSMatrix(const char* nameAndScheme,
            const int test,
            const char* mesh,
            const int nrow,
            const int ncol,
            const int nz,
            const char* filename)
   : nameAndScheme_( nameAndScheme ),
     testNumber_( test ),
     mesh_( mesh ),
     nRows_( nrow ),
     nColumns_( ncol ),
     nonZeros_( nz ),
     matrixMem_( 0 ),
     memUsage_( 0 ),
     coloffset_( 0 ),
     filename_( filename ),
     verbose_( false ),
     first_( true ),
     istlData_( 0 )
  {
    matrixMem_ = getAllocMem();
    allocateMemory();
  }

  //! default constructor
  CRSMatrix()
   : matrix_( 0 ),
     rows_( 0 ),
     columns_( 0 ),
     rhs_( 0 ),
     solution_( 0 ),
     nameAndScheme_( "" ),
     testNumber_( -1 ),
     mesh_( "" ),
     nRows_( 0 ),
     nColumns_( 0 ),
     nonZeros_( 0 ),
     matrixMem_( 0 ),
     memUsage_( 0 ),
     coloffset_( 0 ),
     filename_( ""),
     verbose_( false ),
     first_( true ),
     istlData_( 0 )
  {
  }

  ~CRSMatrix()
  {
    delete [] columns_;
    delete [] rows_;
    delete [] matrix_;

    delete [] rhs_;
    delete [] solution_;
    delete [] guess_;

    delete istlData_;
  }

  double solve(const std::string& parametername )
  {
    // clear numbers
    numbers_.resize( 0 );

    assert( parametername != "" );

    int verb = 0 ;
    Dune::readMyParameter(parametername, "Verbose", verb , true ) ;
    verbose_ = ( verb > 1 ) ? true : false ;
    const bool verboseOutput = ( verb > 0 ) ? true : false ;

    int mthd = (int) umfpack ;
    if( ! Dune::readMyParameter(parametername, "Solver", mthd ) )
      std::cerr << "Couldn't read 'Solver' from parameter file `" << parametername << "' ! " << std::endl ;

    method_t method = ( method_t ) mthd;
    if( method < umfpack || method > oem_cg )
    {
      std::cerr << "ERORR: wrong choice of solver " << method << " valid are " << umfpack << ",...," << oem_cg -1 << std::endl;
      abort();
    }

    int pre = (int) none ;
    if( ! Dune::readMyParameter(parametername, "Preconditioner", pre ) )
      std::cerr << "Couldn't read 'Preconditioner' from parameter file `" << parametername << "' ! " << std::endl ;

    preconder_t preconder = ( preconder_t ) pre ;
    if( preconder < ilu_0 || preconder > none )
    {
      std::cerr << "ERORR: wrong choice of preconditioner " << preconder << " valid are " << ilu_0 << ",...," << none << std::endl;
      abort();
    }

    int calcConditionNumber = 0;
    readMyParameter( parametername, "Conditionnumber", calcConditionNumber, true );

    int doRunTimeComparison = 0;
    readMyParameter( parametername, "RunTimeComparison", doRunTimeComparison, false );

    // create rhs from given vector
    if( doRunTimeComparison )
    {
      /*
      srand((unsigned)time(0));
      int min = 0;
      int max = 1000;
      */
      for( int i=0; i<nColumns_; ++i)
      {
        //double randNumber = min + (rand() % max);
        //solution_[ i ] = std::abs( 1.0/ randNumber );
        solution_[ i ] = 1.0;
        //std::cout << "sol["<<i<<"] = " << solution_[ i ]  << std::endl;
      }

      multOEM( solution_, rhs_ );
      clearVector( solution_ );
    }

    double time = 0.0;
    int iterations = 0;
    /// get initial residual
    std::pair< double , double > initres = calcResidual();

    std::stringstream resultOutput;

    resultOutput << "---------------------------------------------------------" << std::endl;
    resultOutput << "RESULTS for `" << filename_ << "' " << std::endl;
    resultOutput << "---------------------------------------------------------" << std::endl;
    if( testNumber_ > 0 )
    {
      resultOutput << "Contributor and scheme: " << nameAndScheme_ << std::endl;
      resultOutput << "Test: " << testNumber_ << std::endl;
      resultOutput << "Mesh: " << mesh_ << std::endl;
      resultOutput << "---------------------------------------------------------" << std::endl;
    }
    resultOutput << "Problem size:" << std::endl;
    resultOutput << "1) unknowns:                   " << nColumns_ << std::endl;
    resultOutput << "2) non-zero Matrix entries:    " << nonZeros_ << std::endl;
    resultOutput << "3) entries in blocks of size:  " << blockSize << std::endl;

    resultOutput << "Solver + Preconditioner: "<< solverName[ method ];
    if( method == umfpack ) resultOutput << std::endl;
    else resultOutput << " + " << preconderName[ preconder ];

    double memoryUsed = 0;
    double memoryRusage = 0;

    //double matrixMemUsage = (nonZeros_ * sizeof( double )) +
    //                        (nonZeros_ * sizeof( int )) +
    //                        (nRows_ * sizeof( int ));

    if( method > umfpack && method < petsc_cg )
    {
      if (first_ && istlData_)
      {
        delete istlData_;
        istlData_ = 0;
      }
      std::pair<double,int> info =
        solveISTL<blockSize> (
                       parametername,
                       method,
                       preconder,
                       nRows_,
                       nColumns_,
                       matrix_,
                       rows_,
                       columns_,
                       rhs_,
                       solution_,
                       resultOutput,
                       istlData_,
                       verbose_);

      time = info.first;
      iterations = info.second;

      // substract matrix memory used because matrix is stored in ISTL format
      memoryUsed = getAllocMem() - matrixMem_ ;
      memoryRusage = computeMemoryUsage( matrixMem_ , -1.0 );
    }
    else if( method == umfpack )
    {
      time = solveUMF( nRows_,
                       nColumns_,
                       matrix_,
                       rows_,
                       columns_,
                       rhs_,
                       solution_,
                       memoryUsed,
                       verbose_ );
      // add matrix memory to taht from UMFPACK
      //memoryUsed += matrixMem_ ;
      memoryUsed += getAllocMem() + matrixMem_ ;
      memoryRusage = computeMemoryUsage( matrixMem_ , 0.0 );
    }
    else if( method >= petsc_cg && method <= petsc_gmres )
    {
      memoryUsed = -matrixMem_;
      time = solvePETSc( parametername,
                         method,
                         preconder,
                         blockSize,
                         nRows_,
                         nColumns_,
                         matrix_,
                         rows_,
                         columns_,
                         rhs_,
                         solution_,
                         resultOutput,
                         memoryUsed, // get mem usage from PETSc
                         iterations,
                         verbose_ );

      memoryRusage = computeMemoryUsage( matrixMem_, -1.0 );
    }
    /*
    else if( method == oem_cg || method == oem_gmres )
    {
      time = solveOEM(nRows_, *this, rhs_, solution_, method==oem_gmres , verbose_ );
      // substract matrix memory used because matrix is stored in ISTL format
      memoryUsed = computeMemoryUsage(  matrixMem_, 0.0 );
    }
    */

    // calculate residual
    std::pair< double , double > res = calcResidual();
    resultOutput << "Residual : || b - Ax ||_inf                = " << res.second << std::endl;
    std::cout << "Reduction:                                 = " << res.second - initres.second << std::endl;

    clearVector( guess_ );
    std::pair< double , double > bres = calcError( nRows_, rhs_, guess_ );

    resultOutput << "rel. Res.: || b - Ax ||_inf / || b ||_inf  = " << res.second / bres.second << std::endl;

    // make a unique filename
    std::string filepref ( temporaryFileName() );

    // write solution to a file
    writeSolution( filepref, nColumns_, solution_ );

    numbers_.push_back( time );
    numbers_.push_back( iterations );

    if( calcConditionNumber )
    {
      conditionNumber( resultOutput , parametername );
    }
    else
      resultOutput << "Estimated condition number:   not calculated" << std::endl;

    resultOutput << "CPU time to solve in seconds: " << time << std::endl;
    resultOutput << "Memory used in MB: " << memoryUsed << " (program internal)" << std::endl;
    if( memoryRusage > 0 )
      resultOutput << "Memory used in MB: " << memoryRusage << " (rusage.ru_maxrss)" << std::endl;
    resultOutput << "---------------------------------------------------------" << std::endl;

    std::string filename ( filepref );
    filename += ".txt";

    {
      std::ofstream file ( filename.c_str() );
      if( file ) file << resultOutput.str();
    }

    if( verboseOutput )
      std::cout << resultOutput.str();

    //std::cout << "MyMatrix mem usage in MB: " << matrixMemUsage/1024.0/1024.0 << std::endl;

    return time;
  }

  // read matrix from xdr file
  void readXDR(rw_int_t* rw_int, rw_double_t* rw_double)
  {
    readWriteMatrixAndRhs(rw_int, rw_double, nRows_, nColumns_,
                          rows_, columns_, matrix_, rhs_, coloffset_ );

    assert( nonZeros_ == rows_[ nRows_ ] );
  }

  // return condition number
  void conditionNumber( std::ostream& out , const std::string& parametername ) const
  {
    double min, max, cond;
    //std::cout << "Condition calculated by LAPACK: " << calculateConditionNumber( *this ) << std::endl;
    calculateConditionNumber( parametername, nRows_, matrix_, rows_, columns_,
                              out,  min, max, cond, verbose_ );
    // store eigenvalues
    numbers_.push_back( min );
    numbers_.push_back( max );
    numbers_.push_back( cond );
  }

  // calculate || Ax - b ||_inf
  std::pair<double,double> calcResidual()
  {
    multOEM( solution_, guess_ );
    std::pair<double,double> err = calcError( nRows_, rhs_, guess_ );

    clearVector( guess_ );
    return err;
  }

  // calculate || x - y ||_inf
  std::pair<double,double> calcError(const int n, const double* x, const double* y) const
  {
    double sum = 0;
    double inf = 0;
    for( int i=0; i<n; ++i)
    {
      //std::cout << "calc["<<i<<"] = " << x[i] << "   " << y[i] << std::endl;
      double val = std::abs(x[i] - y[i]);
      inf = std::max( inf, val );
      sum += ( val * val );
    }
    return std::make_pair( sum, inf );
  }

  // number of rows
  int rows() const { return nRows_; }
  // number of columns
  int cols() const { return nColumns_; }

  // return matrix entry (row,col)
  double operator ()( const int row, const int col ) const
  {
    assert( row < rows() );
    const int colmax = rows_[ row + 1 ];
    for( int c = 0; c < colmax; ++ c)
    {
      if( columns_[ c ] == col ) return matrix_[ c ];
    }
    return 0.0;
  }

  bool denseMatrix( double* matrixVector ) const
  {
    // initialize with zero
    std::memset( matrixVector, 0, nRows_ * nColumns_ * sizeof( double ));

    bool symmetric = true ;
    // fill matrix entries
    for(int row = 0; row < nRows_; ++row )
    {
      const int maxcol = rows_[ row + 1 ];
      for(int col = rows_[ row ]; col < maxcol; ++col)
      {
        // fill entry
        matrixVector[ nColumns_ * row + columns_[ col ] ] = matrix_[ col ];
        // check symmetry
        if( columns_[ col ] < row )
        {
          const int rowcol = nColumns_ * columns_[ col ] + row ;
          // if difference is to big, matrix is not symmetric
          if( std::abs( matrix_[ col ] - matrixVector[ rowcol ] ) > 1e-10 )
            symmetric = false ;
        }
      }
    }
    return symmetric ;
  }

  // calculate A*x = b
  void multOEM(const double* x, double* b) const
  {
    for(int row = 0; row<nRows_; ++row)
    {
      b[ row ] = x[ row ];
    }
    for(int row = 0; row<nRows_; ++row)
    {
      b[ row ] = 0;
      const int maxcol = rows_[ row + 1 ];
      for(int col = rows_[ row ]; col < maxcol; ++col)
      {
        b[ row ] += matrix_[ col ] * x[ columns_[ col ] ];
      }
    }
  }

  // calculate  < x , y >
  double ddotOEM(const double* x, const double* y) const
  {
    double dot = 0;
    for(int row=0; row<nRows_; ++row)
    {
      dot += x[ row ] * y[ row ];
    }
    return dot;
  }

protected:
  std::string temporaryFileName ()
  {
    std::string filetemp ( filename_ );
    filetemp += ".output";
    return filetemp ;
  }

  void writeSolution(const std::string& fname,
                     const int numberOfUnknowns,
                     const double* solution)
  {
    std::string filename ( fname );
    filename += ".xdr";

    XDR xdrs;
    FILE* file = fopen(filename.c_str(), "wb");

    /* check file */
    if( ! file )
    {
      std::cerr << "Couldn't open file `" << filename << "' for writing !" << std::endl;
      abort();
    }

    /* create XDR stream for reading */
    xdrstdio_create( &xdrs, file, XDR_ENCODE);

    /* set xdr pointer */
    setXDRWritePointer( & xdrs );

    /* read dimensions from file */
    readWriteSolution( readwrite_int_xdr,
                       readwrite_double_xdr,
                       numberOfUnknowns,
                       (double *) solution);

    /* destroy XDR stream */
    xdr_destroy( &xdrs );

    /* close file */
    fclose( file );
  }
};
#endif
