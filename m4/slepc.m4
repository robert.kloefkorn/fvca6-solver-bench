# searches for slepc-headers and libs

AC_DEFUN([DUNE_PATH_SLEPC],[
  AC_REQUIRE([AC_PROG_CC])
  AC_REQUIRE([DUNE_PATH_PETSC])

  AC_ARG_WITH(slepc,
    AC_HELP_STRING([--with-slepc=PATH],[directory with SLEPC inside]))
  AC_ARG_WITH(slepc-includedir,
    AC_HELP_STRING([--with-slepc-includedir=PATH],[directory with SLEPC headers inside]))
  AC_ARG_WITH(slepc-libdir,
    AC_HELP_STRING([--with-slepc-libdir=PATH],[directory with SLEPC libraries inside]))

# store old values
ac_save_LDFLAGS="$LDFLAGS"
ac_save_CPPFLAGS="$CPPFLAGS"
ac_save_LIBS="$LIBS"

SLEPCYES=0
## do nothing if no --with-slepc was supplied
if test x$with_slepc != x && test x$with_slepc != xno ; then
  SLEPCYES=1
fi
if test x$with_slepc_includedir != x && test x$with_slepc_includedir != xno ; then
  SLEPCYES=1
fi
if test x$with_slepc_libdir != x && test x$with_slepc_libdir != xno ; then
  SLEPCYES=1
fi
if test x$HAVE_PETSC = x ; then 
  AC_MSG_WARN([PETSC not found, therefore no SLEPC support available!])
  SLEPCYES=0
fi 

if test x$SLEPCYES = x1 ; then

  # is --with-slepc=bla used?
  if test "x$with_slepc" != x ; then
    SLEPCROOT=`cd $with_slepc && pwd`
	  if ! test -d $SLEPCROOT;  then
      AC_MSG_WARN([SLEPC directory $with_slepc does not exist])
  	fi

    if test "x$SLEPCROOT" = x; then
      # use some default value...
      SLEPCROOT="/usr/local/slepc"
    fi

    SLEPC_LIB_PATH="$SLEPCROOT/lib"
    SLEPC_INCLUDE_PATH="$SLEPCROOT/include"
    SLEPC_CONF_DIR="$SLEPCROOT/conf"
    SLEPC_PACKAGES_LIBS=`grep "PACKAGES_LIBS" $SLEPCROOT/conf/slepcvariables | cut -d "=" -f2`
  else 
    if test "x$with_slepc_includedir" != x ; then 
      SLEPC_INCLUDE_PATH=`cd $with_slepc_includedir && pwd`
      if ! test -d $SLEPC_INCLUDE_PATH;  then
        AC_MSG_WARN([SLEPC directory $with_slepc_includedir does not exist])
      fi
    fi
    if test "x$with_slepc_libdir" != x ; then 
      SLEPC_LIB_PATH=`cd $with_slepc_libdir && pwd`
      if ! test -d $SLEPC_LIB_PATH;  then
        AC_MSG_WARN([SLEPC directory $with_slepc_libdir does not exist])
      fi
    fi
    UMFAMD_LIB_PATH=$SLEPC_LIB_PATH
  fi

  # set variables so that tests can use them
  REM_CPPFLAGS=$CPPFLAGS


  LDFLAGS="$LDFLAGS $PETSC_LDFLAGS $PETSC_LIBS -L$SLEPC_LIB_PATH $SLEPC_PACKAGES_LIBS"
  SLEPC_INC_FLAG="-I$SLEPC_INCLUDE_PATH -DENABLE_SLEPC=1"
  CPPFLAGS="$CPPFLAGS $PETSC_CPPFLAGS $SLEPC_INC_FLAG $MPI_CPPFLAGS"

  # check for header
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([slepc.h], 
     [SLEPC_CPPFLAGS="$SLEPC_INC_FLAG"
      HAVE_SLEPC="1"],
      AC_MSG_WARN([slepc.h not found in $SLEPC_INCLUDE_PATH]))
   
  CPPFLAGS="$REM_CPPFLAGS"
  REM_CPPFLAGS=

  REM_LDFLAGS=$LDFLAGS

  # if header is found...
  if test x$HAVE_SLEPC = x1 ; then
    AC_CHECK_LIB(slepc,[main],
    [SLEPC_LIBS="-lslepc $PETSC_LIBS"
     SLEPC_LDFLAGS="-L$SLEPC_LIB_PATH $SLEPC_PACKAGES_LIBS $PETSC_LDFLAGS"],
	  [HAVE_SLEPC="0"
	  AC_MSG_WARN(libslepc not found!)])
  fi

  LDFLAGS=$REM_LDFLAGS
  AC_LANG_POP

## end of slepc check (--without wasn't set)
fi

# survived all tests?
if test x$HAVE_SLEPC = x1 ; then
  AC_SUBST(SLEPC_LIBS, $SLEPC_LIBS)
  AC_SUBST(SLEPC_LDFLAGS, $SLEPC_LDFLAGS)
  AC_SUBST(SLEPC_CPPFLAGS, $SLEPC_CPPFLAGS)
  AC_DEFINE(HAVE_SLEPC, ENABLE_SLEPC,
    [This is only true if slepc-library was found by configure 
     _and_ if the application uses the SLEPC_CPPFLAGS])

  # add to global list
  DUNE_PKG_LDFLAGS="$SLEPC_LDFLAGS $DUNE_PKG_LDFLAGS"
  DUNE_PKG_LIBS="$SLEPC_LIBS $DUNE_PKG_LIBS"
  DUNE_PKG_CPPFLAGS="$SLEPC_CPPFLAGS $DUNE_PKG_CPPFLAGS"
  ALL_PKG_LDFLAGS="$SLEPC_LDFLAGS $ALL_PKG_LDFLAGS"
  ALL_PKG_LIBS="$SLEPC_LIBS $ALL_PKG_LIBS"
  ALL_PKG_CPPFLAGS="$SLEPC_CPPFLAGS $ALL_PKG_CPPFLAGS"

  # set variable for summary
  with_slepc="yes ($SLEPCROOT)"

else
  AC_SUBST(SLEPC_LIBS, "")
  AC_SUBST(SLEPC_LDFLAGS, "")
  AC_SUBST(SLEPC_CPPFLAGS, "")

  # set variable for summary
  with_slepc="no"
fi
  
# also tell automake
AM_CONDITIONAL(SLEPC, test x$HAVE_SLEPC = x1)

# reset old values
LIBS="$ac_save_LIBS"
CPPFLAGS="$ac_save_CPPFLAGS"
LDFLAGS="$ac_save_LDFLAGS"

  DUNE_ADD_SUMMARY_ENTRY([SLEPC],[$with_slepc])
])
