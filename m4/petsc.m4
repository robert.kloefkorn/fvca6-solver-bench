# searches for petsc-headers and libs

AC_DEFUN([DUNE_PATH_PETSC],[
  AC_REQUIRE([AC_PROG_CC])

  AC_ARG_WITH(petsc,
    AC_HELP_STRING([--with-petsc=PATH],[directory with PETSC inside]))
  AC_ARG_WITH(petsc-includedir,
    AC_HELP_STRING([--with-petsc-includedir=PATH],[directory with PETSC headers inside]))
  AC_ARG_WITH(petsc-libdir,
    AC_HELP_STRING([--with-petsc-libdir=PATH],[directory with PETSC libraries inside]))

# store old values
ac_save_LDFLAGS="$LDFLAGS"
ac_save_CPPFLAGS="$CPPFLAGS"
ac_save_LIBS="$LIBS"

PETSCYES=0
## do nothing if no --with-petsc was supplied
if test x$with_petsc != x && test x$with_petsc != xno ; then
  PETSCYES=1
fi
if test x$with_petsc_includedir != x && test x$with_petsc_includedir != xno ; then
  PETSCYES=1
fi
if test x$with_petsc_libdir != x && test x$with_petsc_libdir != xno ; then
  PETSCYES=1
fi

if test x$PETSCYES = x1 ; then

  # is --with-petsc=bla used?
  if test "x$with_petsc" != x ; then
    PETSCROOT=`cd $with_petsc && pwd`
	  if ! test -d $PETSCROOT;  then
      AC_MSG_WARN([PETSC directory $with_petsc does not exist])
  	fi

    if test "x$PETSCROOT" = x; then
      # use some default value...
      PETSCROOT="/usr/local/petsc"
    fi

    PETSC_LIB_PATH="$PETSCROOT/lib"
    PETSC_INCLUDE_PATH="$PETSCROOT/include"
    PETSC_CONF_DIR="$PETSCROOT/conf"
    PETSC_PACKAGES_LIBS=`grep "PACKAGES_LIBS" $PETSCROOT/conf/petscvariables | cut -d "=" -f2`
  else 
    if test "x$with_petsc_includedir" != x ; then 
      PETSC_INCLUDE_PATH=`cd $with_petsc_includedir && pwd`
      if ! test -d $PETSC_INCLUDE_PATH;  then
        AC_MSG_WARN([PETSC directory $with_petsc_includedir does not exist])
      fi
    fi
    if test "x$with_petsc_libdir" != x ; then 
      PETSC_LIB_PATH=`cd $with_petsc_libdir && pwd`
      if ! test -d $PETSC_LIB_PATH;  then
        AC_MSG_WARN([PETSC directory $with_petsc_libdir does not exist])
      fi
    fi
    UMFAMD_LIB_PATH=$PETSC_LIB_PATH
  fi

  # set variables so that tests can use them
  REM_CPPFLAGS=$CPPFLAGS


  LDFLAGS="$LDFLAGS -L$PETSC_LIB_PATH $PETSC_PACKAGES_LIBS"
  PETSC_INC_FLAG="-I$PETSC_INCLUDE_PATH -DENABLE_PETSC=1"
  CPPFLAGS="$CPPFLAGS $PETSC_INC_FLAG $MPI_CPPFLAGS"

  # check for header
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([petsc.h], 
     [PETSC_CPPFLAGS="$PETSC_INC_FLAG"
      HAVE_PETSC="1"],
      AC_MSG_WARN([petsc.h not found in $PETSC_INCLUDE_PATH]))
   
  CPPFLAGS="$REM_CPPFLAGS"
  REM_CPPFLAGS=

  REM_LDFLAGS=$LDFLAGS

  # if header is found...
  if test x$HAVE_PETSC = x1 ; then
    AC_CHECK_LIB(petsc,[main],
    [PETSC_LIBS="-lpetsc"
     PETSC_LDFLAGS="-Wl,-rpath=$PETSC_LIB_PATH -L$PETSC_LIB_PATH $PETSC_PACKAGES_LIBS"],
	  [HAVE_PETSC="0"
	  AC_MSG_WARN(libpetsc not found!)])
  fi

  LDFLAGS=$REM_LDFLAGS
  AC_LANG_POP

## end of petsc check (--without wasn't set)
fi

# survived all tests?
if test x$HAVE_PETSC = x1 ; then
  AC_SUBST(PETSC_LIBS, $PETSC_LIBS)
  AC_SUBST(PETSC_LDFLAGS, $PETSC_LDFLAGS)
  AC_SUBST(PETSC_CPPFLAGS, $PETSC_CPPFLAGS)
  AC_DEFINE(HAVE_PETSC, ENABLE_PETSC,
    [This is only true if petsc-library was found by configure 
     _and_ if the application uses the PETSC_CPPFLAGS])

  # add to global list
  DUNE_PKG_LDFLAGS="$PETSC_LDFLAGS $DUNE_PKG_LDFLAGS"
  DUNE_PKG_LIBS="$PETSC_LIBS $DUNE_PKG_LIBS"
  DUNE_PKG_CPPFLAGS="$PETSC_CPPFLAGS $DUNE_PKG_CPPFLAGS"
  ALL_PKG_LDFLAGS="$PETSC_LDFLAGS $ALL_PKG_LDFLAGS"
  ALL_PKG_LIBS="$PETSC_LIBS $ALL_PKG_LIBS"
  ALL_PKG_CPPFLAGS=" $PETSC_CPPFLAGS $ALL_PKG_CPPFLAGS"

  # set variable for summary
  with_petsc="yes ($PETSCROOT)"

else
  AC_SUBST(PETSC_LIBS, "")
  AC_SUBST(PETSC_LDFLAGS, "")
  AC_SUBST(PETSC_CPPFLAGS, "")

  # set variable for summary
  with_petsc="no"
fi
  
# also tell automake
AM_CONDITIONAL(PETSC, test x$HAVE_PETSC = x1)

# reset old values
LIBS="$ac_save_LIBS"
CPPFLAGS="$ac_save_CPPFLAGS"
LDFLAGS="$ac_save_LDFLAGS"

  DUNE_ADD_SUMMARY_ENTRY([PETSC],[$with_petsc])
])
