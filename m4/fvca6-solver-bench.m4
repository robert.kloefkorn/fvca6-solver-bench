# Additional checks needed to build the module
AC_DEFUN([FVCA6_SOLVER_BENCH_CHECKS],[
  AC_REQUIRE([DUNE_PATH_UMFPACK])
  AC_REQUIRE([DUNE_PATH_PETSC])
  AC_REQUIRE([DUNE_PATH_SLEPC])
])
# Additional checks needed to find the module
AC_DEFUN([FVCA6_SOLVER_BENCH_CHECK_MODULE],
[
  DUNE_CHECK_MODULES([fvca6-solver-bench],[solverbench/solverbench.hh])
])
