#!/bin/bash

# this script downloads the necessary set of dune modules
# for the fvca6 solver comparison suite

#change appropriately, i.e. 2.3, 2.4 or empty which refers to master
DUNEVERSION=2.3
FLAGS="-O3 -DNDEBUG"

DUNEMODULES="dune-common dune-istl"
MODDIR=`pwd`

# build flags for all DUNE and OPM modules
# change according to your needs
if ! test -f config.opts ; then
echo "MAKE_FLAGS=-j4
USE_CMAKE=no
CONFIGURE_FLAGS=\"CXXFLAGS=\\\"$FLAGS\\\" \\
  --cache-file=../cache.config \\
  --disable-documentation \\
  --enable-experimental-grid-extensions \\
  --enable-parallel \\
  --with-petsc=$MODDIR/petsc3.1-p8 \\
  --with-slepc=$MODDIR/slepc3.1-p6 \\
  --enable-fieldvector-size-is-method\"" > config.opts
fi

DUNEBRANCH=
if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi

# get all dune modules necessary
for MOD in $DUNEMODULES ; do
  git clone $DUNEBRANCH http://git.dune-project.org/repositories/$MOD
done

# clone fvca6-solver-bench
git clone https://users.dune-project.org/repositories/projects/fvca6-solver-bench.git

# build all DUNE and OPM modules in the correct order
./dune-common/bin/dunecontrol --opts=config.opts all
