#!/bin/bash 
AUTHORS=$1
FILE=$2

TMPNAME=`echo $FILE | cut -d "/" -f 3`
if [ "$TMPNAME" == "example_C.c" ]; then 
  exit 0
fi  
if [ "$TMPNAME" == "example_F.f90" ]; then 
  exit 0
fi  

OUT=tmp.cc.$LOGNAME 
echo "/**************************************************************************" > $OUT
echo " " >> $OUT 
cat $AUTHORS >> $OUT 
echo " " >> $OUT 
echo "**************************************************************************/" >> $OUT
cat $FILE >> $OUT
cp $OUT $FILE 
rm -f $OUT
