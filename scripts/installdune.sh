#!/bin/sh
#
# (C) 2010 Robert Kloefkorn
#
################################################################

# check for parameters 
if test x$1 != x ; then 
  export MAKE_NEW="1"
else 
  echo "Usage: $0 <dune-common|dune-istl|solver-bench|all>"
  echo "The flag invokes setup to make installations again."
fi

VERSION=2.0
if ! test -f dune-common-$VERSION.tar.gz ; then 
  # check whether a new install should be done 
  echo "downloading dune-common-$VERSION.tar.gz of the dune-core modules"
  wget http://www.dune-project.org/download/$VERSION/dune-common-$VERSION.tar.gz
fi  
if ! test -f dune-istl-$VERSION.tar.gz ; then 
  echo "downloading dune-istl-$VERSION.tar.gz of the dune-core modules"
  wget http://www.dune-project.org/download/$VERSION/dune-istl-$VERSION.tar.gz
fi

# check for modules 
case "$1" in 
dune-common) 
  INSTALL_COMMON=1 ;;
dune-istl) 
  INSTALL_ISTL=1 ;;
solver-comp) 
  SOLVER_COMP=1 ;;
solver-bench) 
  SOLVER_COMP=1 ;;
*)   
  INSTALL_COMMON=1 
  INSTALL_ISTL=1 
  SOLVER_COMP=1
esac   

# configure call (modify DUNEDIR and MODDIR) 
callConfigure() 
{
  BASEDIR=`pwd`
  # global install path 
  DUNEDIR=$BASEDIR/Dune
    

  # change if necessary 
  UMFPACKDIR=`pwd`/UMFPACK
  # change if necessary 
  PETSCDIR=`pwd`/petsc3.1-p5
  # change if necessary 
  SLEPCDIR=`pwd`/slepc3.1-p4

  if test -d $UMFPACKDIR ; then 
    UMFPACKDIR="--with-umfpack=$UMFPACKDIR"
  else 
    UMFPACKDIR=""
  fi 

  if test -d $PETSCDIR ; then 
    PETSCDIR="--with-petsc=$PETSCDIR"
  else 
    PETSCDIR=""
  fi 

  if test -d $SLEPCDIR ; then 
    SLEPCDIR="--with-slepc=$SLEPCDIR"
  else 
    SLEPCDIR=""
  fi 

  # enter directory 
  if ! test -d $1 ; then
    echo "ERROR: $1 does not exsist!"
    exit 1
  fi
  cd $1 ;

  echo "#############################################################"
  echo "Called with: $@"
  echo " "
  echo "Entering directory:"
  echo " "
  echo "`pwd`"
  echo "and calling configure!"
  echo " "
  echo "#############################################################"

  if test x$2 != x ; then
    if ! test -d $DUNEDIR ; then 
      DUNECOMMON="--with-dune-common=$BASEDIR/dune-common-$VERSION/"
    else  
      DUNECOMMON="--with-dune-common=$DUNEDIR"
    fi  
  fi

  if test x$3 != x ; then
    if ! test -d $DUNEDIR ; then
      DUNEISTL="--with-dune-istl=$BASEDIR/dune-istl-$VERSION/"
    else  
      DUNEISTL="--with-dune-istl=$DUNEDIR"
    fi  
  fi

  # change these flags to your appropriate choice
  STDFLAGS="-O3 -funroll-loops -finline-functions -DNDEBUG"

  ./configure CXX=g++ CXXFLAGS="$STDFLAGS" \
    --prefix=$DUNEDIR \
    --enable-parallel \
    --disable-documentation \
    --disable-compilercheck \
    --disable-mpiruntest \
    $UMFPACKDIR $PETSCDIR $SLEPCDIR $DUNECOMMON $DUNEISTL

  # other possible flags   
  #
  #  --with-alugrid=$MODULEDIR/ALUGrid-1.14 \
  #  --with-alberta=$MODULEDIR/alberta-2.0 \
  #  --disable-documentation \
  #  --with-ug=$MODULEDIR/UG \
  #  --disable-mpiruntest \
  #  --disable-compilercheck \

  # make clean first (for second installation)
  # this is not working since dune-*-1.2.1 documentation is buggy
  # make clean

  if [ "$4" == "" ] ; then 
    # make (build library and install) 
    make install
  else 
    make 
  fi 

  # go back 
  cd ../
}
  
# function to unpack tar-ball
extractDirName() 
{
  # remove last 8 characters (ie. ".tar.gz")
  WC_COUNT=`echo $1 | wc -c`
  WC_COUNT=`expr $WC_COUNT - 8`

  # created directory 
  TARDIR="`echo $1 | cut -c1-$WC_COUNT `"

  # return value 
  echo $TARDIR
}

# install a module 
installModule() 
{
  # create package tar name 
  PACKAGE_NAME=`echo $1`"*.tar.gz"
  # extract module name 
  MODULE_NAME=`find . -maxdepth 1 -name "$PACKAGE_NAME"`
  # get directory name 
  export MODULE_DIR=$(extractDirName $MODULE_NAME)

  # check whether dir exists 
  if ! test -d $MODULE_DIR ; then 
    # extract package 
    tar zxvf $MODULE_NAME
    # call configure later 
    export MAKE_NEW="1"
  fi   

  # only call configure if really a make new should be done 
  if test x$MAKE_NEW != x; then 
    # shift parameter to remove first 
    shift 
    # call configure 
    (callConfigure "$MODULE_DIR" $*)  
  fi
}

if test "x$INSTALL_COMMON" = "x1" ; then 
  # install common module 
  (installModule "dune-common")
fi      

if test "x$INSTALL_ISTL" = "x1" ; then 
  # install istl module 
  (installModule "dune-istl" "dune-common")
fi  

if test "x$SOLVER_COMP" = "x1" ; then 
  # install istl module 
  (installModule "solver-bench" "dune-istl" "dune-common")
fi  

exit 0
