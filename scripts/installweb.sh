#!/bin/bash

VERSION=$1

if test -d $HOME/www/3dbenchmark/solver-bench ; then 
  WEB_INSTALL_DIR=$HOME/www/3dbenchmark/solver-bench
  rm $WEB_INSTALL_DIR/examples-*.tar.gz 
  rm $WEB_INSTALL_DIR/solver-bench-*.tar.gz 
  cp solver-bench-$VERSION.tar.gz $WEB_INSTALL_DIR
  cp examples-$VERSION.tar.gz $WEB_INSTALL_DIR 
  OLDPATH=`pwd`
  cd $WEB_INSTALL_DIR 
  ln -s solver-bench-$VERSION.tar.gz solver-bench-latest.tar.gz  
  ln -s examples-$VERSION.tar.gz examples-latest.tar.gz  
  cd $OLDPATH 
  cp scripts/installdune.sh $WEB_INSTALL_DIR
  cp scripts/umfpack_installer.sh $WEB_INSTALL_DIR
  cp scripts/petsc_slepc_installer.sh $WEB_INSTALL_DIR

  cd doc 
  VERS1=`grep "examples\-" documentation | cut -d "/" -f 9 | cut -d "-" -f 2 | cut -d "t" -f 1 | cut -d "." -f 2`
  VERS2=`echo $VERSION | cut -d "." -f 2`

  if [ "$VERS1" != "$VERS2" ] ; then 
    replace "$VERS1" "$VERS2" -- documentation 
    echo "Versions not equal" 
  fi  

  doxygen Doxyfile 

  cp -r html $WEB_INSTALL_DIR

  cd ../
fi  
