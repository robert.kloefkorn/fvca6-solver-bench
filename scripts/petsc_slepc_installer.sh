#!/bin/bash

OPTIMFLAGS="-O3 -funroll-loops -finline-functions -DNDEBUG"
PETSCVERSION=3.1-p5
PETSC_INSTALL_DIR=`pwd`/petsc$PETSCVERSION

SLEPCVERSION=3.1-p4

if [ "$1" == "all" ] ; then 
  INSTALL_PETSC=1
  ISNTALL_SLEPC=1
fi 

if [ "$1" == "petsc" ] ; then 
  INSTALL_PETSC=1
fi 

if [ "$1" == "slepc" ] ; then 
  INSTALL_SLEPC=1
fi 

# download packages 
if ! test -e petsc-$PETSCVERSION.tar.gz ; then 
  wget http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-$PETSCVERSION.tar.gz
  tar zxvf petsc-$PETSCVERSION.tar.gz 
  INSTALL_PETSC=1
fi  

if ! test -e slepc-$SLEPCVERSION.tgz ; then 
  wget http://www.grycap.upv.es/slepc/download/distrib/slepc-$SLEPCVERSION.tgz 
  tar zxvf slepc-$SLEPCVERSION.tgz
  INSTALL_SLEPC=1
fi  

if [ "$INSTALL_PETSC" == "1" ] ; then 
  cd petsc-$PETSCVERSION
  unset PETSC_DIR

  ./configure --prefix=$PETSC_INSTALL_DIR --with-x=false --with-debugging=no --CFLAGS=$OPTIMFLAGS

  make 
  make PETSC_DIR=`pwd` install
  cd ../ 

fi  

export PETSC_DIR=$PETSC_INSTALL_DIR
# finished PETSc installation 

if [ "$INSTALL_SLEPC" == "1" ] ; then 
  SLEPC_INSTALL_DIR=`pwd`/slepc$SLEPCVERSION
  cd slepc-$SLEPCVERSION
  export SLEPC_DIR=`pwd`
  ./configure --prefix=$SLEPC_INSTALL_DIR 
  export PETSC_ARCH=installed-petsc
  make 
  make install 
  export SLEPC_DIR=$SLEPC_INSTALL_DIR
  unset PETSC_ARCH
  export SLEPC_DIR=$SLEPC_INSTALL_DIR
  cd ../
fi  
