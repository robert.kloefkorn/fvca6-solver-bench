#!/bin/sh

# compiler flags, change here for all programs 
STDFLAGS="-O3 -funroll-loops -finline-functions"

# check whether a new install should be done 
VERSION=1.2.1
BENCHVERSION=0.1

if ! test -d ./dune-common-$VERSION ; then 
  echo "download version $2 from dune"
  wget http://www.dune-project.org/download/$VERSION/dune-common-$VERSION.tar.gz
fi 

if ! test -d ./dune-istl-$VERSION ; then 
  echo "download version $2 from dune"
  wget http://www.dune-project.org/download/$VERSION/dune-istl-$VERSION.tar.gz
fi

if ! test -d ./benchmark-runtime-$BENCHVERSION ; then 
  wget http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/3dbenchmark/runtime/benchmark-runtime-$BENCHVERSION.tar.gz  
fi 

if test x$1 = x ; then 
  export MAKE_NEW="1"
  echo "Usage: $0 <dune-common|dune-istl|benchmark-runtime>"
  echo "No flag invokes setup to make all installs again."
  sleep 2 
fi

# check for modules 
case "$1" in 
UMFPACK) 
  INSTALL_UMFPACK=1 ;;
dune-common) 
  INSTALL_COMMON=1 ;;
dune-istl) 
  INSTALL_ISTL=1 ;;
benchmark-runtime) 
  INSTALL_COMP=1 ;; 
*)   
  INSTALL_COMMON=1 
  INSTALL_ISTL=1 
  INSTALL_COMP=1
  INSTALL_UMFPACK=1 ;;
esac   

# configure call (modify DUNEDIR and MODDIR) 
callConfigure() 
{
  # modules directory
  MODDIR=`pwd` 
  # global install path 
  DUNEDIR=$MODDIR/Dune

  # enter directory 
  if ! test -d $1 ; then
    echo "ERROR: $1 does not exsist!"
    exit 1
  fi
  cd $1 ;

  echo 
  echo 
  echo "#############################################################"
  echo "Called with: $@"
  echo 
  echo "Entering directory:"
  echo 
  echo "`pwd`"
  echo "and calling configure!"
  echo 
  echo "#############################################################"

  sleep 1 

  if test x$2 != x ; then
  DUNECOMMON="--with-dune-common=$DUNEDIR"
  fi

  if test x$4 != x ; then
  DUNEISTL="--with-dune-istl=$DUNEDIR"
  fi

  # possible other options 
  #  --disable-documentation \
  #  --enable-parallel \

  ./configure CXX=g++ CXXFLAGS="$STDFLAGS" \
    --prefix=$DUNEDIR \
    --disable-mpiruntest \
    --disable-compilercheck \
    --with-umfpack=$MODDIR/UMFPACK \
    $DUNECOMMON

  make clean
  make install

  # go back 
  cd ../
}
  
# function to unpack tar-ball
extractDirName() 
{
  # remove last 8 characters (ie. ".tar.gz")
  WC_COUNT=`echo $1 | wc -c`
  WC_COUNT=`expr $WC_COUNT - 8`

  # created directory 
  TARDIR="`echo $1 | cut -c1-$WC_COUNT `"

  # return value 
  echo $TARDIR
}

# install a module 
installModule() 
{
  # create package tar name 
  PACKAGE_NAME=`echo $1`"*.tar.gz"
  # extract module name 
  MODULE_NAME=`find . -name "$PACKAGE_NAME"`
  # get directory name 
  export MODULE_DIR=$(extractDirName $MODULE_NAME)

  # check whether dir exists 
  if ! test -d $MODULE_DIR ; then 
    # extract package 
    tar zxvf $MODULE_NAME
    # call configure later 
    export MAKE_NEW="1"
  fi   

  # only call configure if really a make new should be done 
  if test x$MAKE_NEW != x; then 
    # shift parameter to remove first 
    shift 
    # call configure 
    (callConfigure "$MODULE_DIR" $*)  
  fi
}


# UMFPACK isntallation 
if test "x$INSTALL_UMFPACK" = "x1" ; then 

if ! test -f ./umfpack_installer.sh ; then 
  wget http://www.mathematik.uni-freiburg.de/IAM/homepages/robertk/3dbenchmark/runtime/umfpack_installer.sh 
  chmod +x ./umfpack_installer.sh 
fi 

./umfpack_installer.sh "$STDFLAGS"

echo 
echo 
echo "**********************************************"
echo "*   UMFPACK installation finished!           *"
echo "*   Starting installation of Dune modules!   *"
echo "**********************************************"
echo 
echo

sleep 4 
fi 

# dune-common installation 
if test "x$INSTALL_COMMON" = "x1" ; then 
  # install common module 
  (installModule "dune-common")
fi      

# dune-istl installation 
if test "x$INSTALL_ISTL" = "x1" ; then 
  # install istl module 
  (installModule "dune-istl" "dune-common")
fi  

# benchmark-runtime installation 
if test "x$INSTALL_COMP" = "x1" ; then 
  # install benchmark-runtime module 
  (installModule "benchmark-runtime" "dune-istl" "dune-common")
fi  

exit 0
