#include <stdlib.h>
#include "writexdr.h"

/********************************************************************
 *
 * C program writing example file
 *
 ********************************************************************/
int main()
{
  /* setup system
     We solve  A * x = b with

          | 2  3  0  0  0 |      | 1 |       |  8 |
          | 3  0  4  0  6 |      | 2 |       | 45 |
      A = | 0 -1 -3  2  0 |  x = | 3 |   b = | -3 |
          | 0  0  1  0  0 |      | 4 |       |  3 |
          | 0  4  2  0  1 |      | 5 |       | 19 |
  */

  const int CSRformat = 0; /* the default C format */
  const int blockSize = 1; /* 1 for most schemes */

  int n = 5 ; /* the number of rows (i.e. number of unknowns ) */
  /* the row starting numbers */
  int Ap [ ] = {0, 2, 5, 8, 9, 12} ;
  /* the real columns numbers */
  int Ai [ ]    = { 0, 1,  0,   2,  4,  1,   2,  3,  2,  1,  2,  4 } ;
  /* the non-zero matrix entries */
  double Ax [ ] = {2., 3., 3., 4., 6., -1., -3., 2., 1., 4., 2., 1.} ;
  /* the right hand side */
  double b [ ] = {8., 45., -3., 3., 19.} ;

  const char* path = "."; /* path to write file to */
  const char* nameAndScheme = "Horst_SuperScheme" ; /* name of contributor and scheme */
  const int test = 1 ; /* number of test, { 1,..., 5 } */
  const char* mesh = "tet.2.msh"; /* { pass the mesh filename used for calculation */

  /* call write function, see writexdr.h and writexdr.c for docu  */
  writeBenchmarkFile( path, nameAndScheme, test, mesh,
                      CSRformat, blockSize, n, n, Ap, Ai, Ax, b );
  return 0;
}
