#ifndef WRITEXDR_H_INCLUDED
#define WRITEXDR_H_INCLUDED

/* system headers */
#include <assert.h>
#include <rpc/types.h>
#include <rpc/xdr.h>

/**
     @defgroup writeXDR Write Benchmark files using XDR
 */

/**
     @defgroup readXDR Reading the Solution using XDR
 */

typedef void rw_int_t(int *);
typedef void rw_double_t(double *);
typedef void rw_string_t(char *, const unsigned int );

/* XDR write and read routines */
void readwrite_int_xdr(int * value);
void readwrite_double_xdr(double * value);
void readwrite_string_xdr(char* value, const unsigned int );

/** \ingroup writeXDR
    \author Robert Kloefkorn
    \brief Implementation of the write routine for calls from \b C/C++ \b programs.
    The implementation writes the number of unknowns, the matrix, and the right hand
    side which then can be read with the program `benchruntime'

    \param[in]  path             path to write file to, e.g. "./"
    \param[in]  nameAndScheme    contributor and name of the scheme
    \param[in]  test             number of the test, valid are { 1,..., 5 }
    \param[in]  mesh             name of the mesh file used for calculation, e.g. "tet.2.msh" or "vmesh_4.msh"
    \param[in]  format           format of CSR storage, values are {0,...,3} \n
                                  \b 0 = offset in rows and columns is zero (default \b C \b format) \n
                                  \b 1 = offset in rows is 0, in columns 1 \n
                                  \b 2 = offset in rows is 1, in columns 0 \n
                                  \b 3 = offset in rows and columns is 1 (default \b Fortran \b format)
    \param[in]  blockSize        block size of DoFs ( \b = \b 1 for most schems such as Finite Volume and Finite Element schemes)
    \param[in]  numberOfRows     number of rows of the matrix
    \param[in]  numberOfColumns  number of columns of the matrix
    \param[in]  rows             offset for each row of the matrix  [0,...,numberOfRows], note length is numberOfRows+1,
                                 the last entries is the number of non-zeros + offset
    \param[in]  columns          real columns number of each matrix entry  [0,...,numberNonZeros-1], length is numberNonZeros
    \param[in]  matrix           matrix entiries [0,...,numberNonZeros-1], length is numberNonZeros
    \param[in]  rhs              right hand side [0,...,numberOfRows-1], length is numberOfRows

    \note Only call this routine from \b C/C++ \b programs.\n
          An example can be found in \ref example_C.c.

*/
void writeBenchmarkFile(const char* path,           /* path to write file to, e.g. `./' */
                        const char* nameAndScheme,  /* contributor and scheme */
                        const int test,             /* number of test , possible set { 1,..., 5 } */
                        const char* mesh,           /* mesh file used for calculation */
                        const int format,           /* format of CSR storage */
                        const int blockSize,        /* block size of DoFs ( = 1 for Finite Volume and Finite Element schemes) */
                        const int numberOfRows,     /* number of rows of the matrix  */
                        const int numberOfColumns,  /* number of columns of the matrix */
                        const int* rows,            /* the offset for each row in the vector matrix */
                        const int* columns,         /* the columns number for each matrix entry */
                        const double* matrix,       /* the matrix entries */
                        const double* rhs);         /* the right hand side */

/** \ingroup writeXDR
    \author Robert Kloefkorn
    \brief Implementation of the write routine for calls from \b Fortran \b programs.
    The implementation writes the number of unknowns, the matrix, and the right hand
    side which then can be read with the program `benchruntime'

    \param[in]  path             path to write file to, e.g. "./"
    \param[in]  nameAndScheme    contributor and name of the scheme
    \param[in]  test             number of the test, valid are { 1,..., 5 }
    \param[in]  mesh             name of the mesh file used for calculation, e.g. "tet.2.msh" or "vmesh_4.msh"
    \param[in]  format           format of CSR storage, values are [0,...,3] \n
                                  \b 0 = offset in rows and columns is zero (default \b C \b format) \n
                                  \b 1 = offset in rows is 0, in columns 1 \n
                                  \b 2 = offset in rows is 1, in columns 0 \n
                                  \b 3 = offset in rows and columns is 1 (default \b Fortran \b format)
    \param[in]  blockSize        block size of DoFs ( \b = \b 1 for most schems such as Finite Volume and Finite Element schemes)
    \param[in]  numberOfRows     number of rows of the matrix
    \param[in]  numberOfColumns  number of columns of the matrix
    \param[in]  rows             offset for each row of the matrix  [0,...,numberOfRows], note length is numberOfRows+1, the last entries is the number of non-zeros + offset
    \param[in]  columns          real columns number of each matrix entry  [0,...,numberNonZeros-1], length is numberNonZeros
    \param[in]  matrix           matrix entiries [0,...,numberNonZeros-1], length is numberNonZeros
    \param[in]  rhs              right hand side [0,...,numberOfRows-1], length is numberOfRows

    \note Only call this routine from \b Fortran \b programs. \n
          The call look as follows:  \n
          \b call \b writebenchmarkfile( CSRformat, filename, blocksize, n, n, rows, columns, matrix, rhs ) \n
          An example can be found in \ref example_F.f90.
*/
void writebenchmarkfile_(const char* path,           /* path to write file to */
                         const char* nameAndScheme,  /* filename of file to write to */
                         const int* test,            /* number of test , possible set { 1,..., 5 } */
                         const char* mesh,           /* mesh file used for calculation */
                         const int* format,          /* format of CSR storage */
                         const int* blockSize,       /* block size of DoFs ( = 1 for Finite Volume and Finite Element schemes) */
                         const int* numberOfRows,    /* number of rows of the matrix  */
                         const int* numberOfColumns, /* number of columns of the matrix */
                         const int* rows,            /* the offset for each row in the vector matrix */
                         const int* columns,         /* the columns number for each matrix entry */
                         const double* matrix,       /* the matrix entries */
                         const double* rhs);         /* the right hand side */


/** \ingroup readXDR
    \author Robert Kloefkorn
    \brief Implementation of the read routine
      for calls from \b C/C++ \b programs

    \param[in]  filename          file name that contains the solution
    \param[in]  numberOfUknowns   number of unknowns to be read (for checking)
    \param[out] solution          pointer to memory where the solution is stored which already has the
                                  correct length given by the discretization grid

    \note Only call this routine from \b C/C++ \b programs.\n
          An example can be found in \ref read_C.c.

 */
void readSolution(const char* filename,  /* filename to read solution from */
                  const int numberOfUknowns, /* number of unknowns to be read (for checking) */
                  double* solution);  /* pointer to memory for solution */

/** \ingroup readXDR
    \author Robert Kloefkorn
    \brief Implementation of the read routine
           for calls from \b Fortran \b programs

    \param[in]  filename          file name that contains the solution
    \param[in]  numberOfUknowns   number of unknowns to be read (for checking)
    \param[out] solution          pointer to memory where the solution is stored which already has the
                                  correct length given by the discretization grid

    \note Only call this routine from \b Fortran \b programs. \n
          The call look as follows:  \n
          \b call \b readsolution( filename, n, solution ) \n
          An example can be found in \ref read_F.f90.
 */
void readsolution_(const char* filename,  /* filename to read solution from */
                   const int* numberOfUknowns, /* number of unknowns to be read (for checking) */
                   double* solution); /* the solution */

/***************************************************************************
 *
 *  Helper functions
 *
 **************************************************************************/

/* read-write of dimensions */
int  readWriteHeader(rw_int_t* rw_int,      /* read-write integer function pointer */
                     rw_double_t* rw_double,/* read-write double function pointer */
                     rw_string_t* rw_string,/* read-write strings function pointer */
                     char** nameAndScheme,  /* filename of file to write to */
                     int* test,             /* number of test , possible set { 1,..., 5 } */
                     char** mesh,           /* mesh file name */
                     int* blockSize,        /* block size of DoFs ( = 1 for Finite Volume and Finite Element schemes) */
                     int* numberNonZeros,   /* number of non-zero entries of the matrix */
                     int* numberOfRows,     /* number of rows of the matrix  */
                     int* numberOfColumns); /* number of columns of the matrix */

/* read-write matrix enrites */
void
readWriteMatrixAndRhs(rw_int_t* rw_int,    /*  read-write integer function pointer */
                      rw_double_t* rw_double, /* read-write double function pointer */
                      const int numberOfRows,    /* number of rows of the matrix  */
                      const int numberOfColumns, /* number of columns of the matrix */
                      int* rows,                 /* the offset for each row in the vector matrix */
                      int* columns,              /* the columns number for each matrix entry */
                      double* matrix,            /* the matrix entries */
                      double* rhs,               /* the right hand side */
                      const int coloffset);      /* offset for indices according to format */

/* read-write solution  */
int readWriteSolution(rw_int_t* rw_int,
                      rw_double_t* rw_double,
                      const int numberOfUnknowns,  /* number of rows of the matrix  */
                      double* solution);           /* the solution */

/* read only block size from given file */
int readBlockSize(const char* filename);   /* filename to read blocksize from */

/* set global xdr pointer (write mode) */
void setXDRWritePointer( XDR* xdrs );
/* set global xdr pointer (read mode) */
void setXDRReadPointer( XDR* xdrs );
#endif
