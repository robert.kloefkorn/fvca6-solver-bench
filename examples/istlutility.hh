#ifndef ISTL_UTILITY_HH
#define ISTL_UTILITY_HH

#include <string>
#include <vector>

extern "C" {
#include "writexdr.h"
}

namespace Dune {

template <class Vector, class BlockVector>
inline void
copyToBlockVector(const Vector& vector,
                  BlockVector& blockVector)
{
  // type of little blocks in BlockVector
  typedef typename BlockVector :: block_type BlockType;

  // copy vector
  const size_t size = blockVector.size();
  for(size_t b = 0, idx = 0; b < size; ++b)
  {
    BlockType& block = blockVector[ b ];
    for(int l=0; l<BlockType :: dimension; ++l, ++idx )
    {
      block[ l ] = vector[ idx ];
    }
  }
}

template <class BlockVector, class Vector>
inline void
copyToVector(const BlockVector& blockVector,
             Vector& vector)
{
  // type of little blocks in BlockVector
  typedef typename BlockVector :: block_type BlockType;

  // copy vector
  const size_t size = blockVector.size();
  for(size_t b = 0, idx = 0; b < size; ++b)
  {
    const BlockType& block = blockVector[ b ];
    for(int l=0; l<BlockType :: dimension; ++l, ++idx )
    {
      vector[ idx ] = block[ l ];
    }
  }
}


template <class MatrixType>
void copyToBlockMatrix(const int numberOfRows,
                       const int numberOfColumns,
                       const double* matrixEntries,
                       const int* rows,
                       const int* columns,
                       MatrixType& matrix)
{
  typedef typename MatrixType :: block_type BlockType;
  enum { blocksize = BlockType :: rows };
  typedef typename MatrixType :: CreateIterator CreateIteratorType;

  // adjust number of rows by blocksize
  const int realRows = (numberOfRows)    / blocksize;
  const int realCols = (numberOfColumns) / blocksize;

  // make sure sizes fit
  assert( realRows * blocksize == numberOfRows );
  assert( realCols * blocksize == numberOfColumns );

  // set size and mode
  matrix.setSize( realRows, realCols );
  matrix.setBuildMode( MatrixType :: row_wise );

  // create matrix pattern
  const CreateIteratorType createend = matrix.createend();
  for(CreateIteratorType create = matrix.createbegin();
      create != createend; ++create)
  {
    int rowIdx = create.index() * blocksize;
    for( int r=0; r<blocksize; ++r, ++rowIdx )
    {
      assert( rowIdx+1 < numberOfRows+1 );
      const int rowend = rows[ rowIdx+1 ];
      // make sure the row is not empty
      assert( rowend > rows[ rowIdx ] );
      for(int i = rows[ rowIdx ]; i<rowend; ++i)
      {
        const int realColumn = columns[ i ] / blocksize;
        // insert real column index
        create.insert( realColumn );
      }
    }
  }

  typedef typename MatrixType :: ColIterator ColIteratorType;
  typedef typename MatrixType :: row_type RowType ;

  for (int r = 0, rowIdx = 0; r < realRows; ++ r)
  {
    RowType& row = matrix[ r ];
    // clear row before inserting entries
    const ColIteratorType endj = row.end();
    for (ColIteratorType j = row.begin(); j != endj; ++j)
    {
      *j = 0;
    }

    // fill matrices
    for(int k=0; k<blocksize; ++k, ++rowIdx )
    {
      for (ColIteratorType j = row.begin(); j != endj; ++j)
      {
        // get real column number
        int realCol = j.index() * blocksize;
        for(int c=0; c<blocksize; ++c, ++realCol)
        {
          assert( rowIdx+1 < numberOfRows +1 );
          const int rowend = rows[ rowIdx+1 ];
          for(int i = rows[ rowIdx ]; i<rowend; ++i)
          {
            // if we find column then store value in block matrix
            if( columns[ i ] == realCol )
            {
              (*j)[ k ][ c ] = matrixEntries[ i ];
            }
          }
        }
      }
    }
  }
} // end copyToBlockMatrix


//! write block matrix and right hand side
//! to benchmark file
template <class MatrixType>
void fillCRSMatrix(const MatrixType& matrix,
                   std::vector< double >& matrixEntries,
                   std::vector< int >& rows,
                   std::vector< int >& columns)
{
  typedef typename MatrixType :: block_type BlockType;
  enum { blocksize = BlockType :: rows };

  const size_t realRows = matrix.N();

  const size_t numberOfRows    = realRows * blocksize;
  assert( numberOfRows == matrix.M() * blocksize );

  typedef typename MatrixType :: ConstColIterator ColIteratorType;
  typedef typename MatrixType :: row_type RowType ;

  // count number of non-zeros
  size_t blockNNZ = 0;
  for (size_t r = 0; r < realRows; ++ r)
  {
    const RowType& row = matrix[ r ];
    blockNNZ += row.size();
  }
  const size_t nnz = blockNNZ * blocksize * blocksize ;

  // adjust sizes
  matrixEntries.resize( nnz );
  columns.resize( nnz );
  rows.resize( numberOfRows + 1 );

  // make sure sizes fit
  assert( realRows * blocksize == numberOfRows );

  int nnzc = 0;
  for (size_t r = 0, rowIdx = 0; r < realRows; ++ r)
  {
    const RowType& row = matrix[ r ];
    const ColIteratorType endj = row.end();

    // fill matrices
    for(int k=0; k<blocksize; ++k, ++rowIdx )
    {
      // store current nnz counter
      rows[ rowIdx ] = nnzc ;

      for (ColIteratorType j = row.begin(); j != endj; ++j)
      {
        // get real column number
        size_t realCol = j.index() * blocksize;
        for(int c=0; c<blocksize; ++c, ++realCol )
        {
          //values[ realCol ] = (*j)[ k ][ c ];
          columns[ nnzc ] = realCol ;
          matrixEntries[ nnzc ] = (*j)[ k ][ c ];
          // increase number of non-zero
          ++ nnzc ;
        }
      }
    }
  }
  // set last row entry
  rows[ numberOfRows ] = nnzc ;
}

/** \brief write block matrix and right hand side
  \param[in]  path             path to write file to, e.g. "./"
  \param[in]  nameAndScheme    contributor and name of the scheme
  \param[in]  test             number of the test, valid are { 1,..., 5 }
  \param[in]  mesh             name of the mesh file used for calculation, e.g. "tet.2.msh" or "vmesh_4.msh"
  \param[in]  matrix           system matrix to store
  \param[in]  rhs              corresponding right hand side
*/
template <class MatrixType, class BlockVectorType >
void writeBlockMatrix(const std::string path,
                      const std::string nameAndScheme,
                      const int test,
                      const std::string mesh,
                      const MatrixType& matrix,
                      const BlockVectorType& rhs )
{
  typedef typename MatrixType :: block_type BlockType;
  enum { blocksize = BlockType :: rows };

  std::vector< double > matrixEntries, rhsEntries ;
  std::vector< int > rows, columns ;

  fillCRSMatrix( matrix, matrixEntries, rows, columns );

  const int numberOfRows    = rows.size() - 1;
  const int numberOfColumns = numberOfRows;

  rhsEntries.resize( numberOfRows );

  // copy right hande side
  copyToVector( rhs, rhsEntries );

  const char * realpath = ( path == "" ) ? "." : path.c_str();
  writeBenchmarkFile(realpath, nameAndScheme.c_str(), test, mesh.c_str(), 0, // CRSFormat
                     blocksize, numberOfRows, numberOfColumns,
                     &rows[0], &columns[0], &matrixEntries[0], &rhsEntries[0] );

} // end copyToBlockMatrix

} // end namespace Dune
#endif
