!------------------------------------------------------------------
!
! This in an example how to use the writebenchmarkfile function in a
! Fortran code. The file writexdr.o has to be linked against the
! Fortran program.
!
!------------------------------------------------------------------


! extern C routines are included as modules 

module writeBenchFile
  interface

    ! write benchmark data, see the files writexdr.h and writexdr.c for docu 
    subroutine writebenchmarkfile(path, nameAndScheme, test, mesh, CSRformat, blockSize, nR, nC, rows, columns, Ax, rhs)

    ! change of these values is not necessary 
    ! dynamically allocatable vectors can be used, see below 
    PARAMETER(n=5,nnz=12)
    character path, nameAndScheme
    integer test
    character mesh
    integer CSRformat, blockSize, nR, nC
    integer rows(n+1), columns(nnz)
    double precision Ax (nnz), rhs (n)

    end subroutine
  end interface
end module

!--------------------------------------------------------
! main program 
!--------------------------------------------------------

program writetest 
use writeBenchFile 

  integer test 
  integer meshLevel 
  integer CSRformat
  integer blockSize
  integer n, nnz

  integer, allocatable, dimension (:):: rows, columns
  double precision, allocatable, dimension (:):: Ax, rhs

  ! path to write file to 
  CHARACTER(LEN=1024) :: path = "."//CHAR(0)
  ! name of the contributor and scheme 
  CHARACTER(LEN=1024) :: nameAndScheme = "Horst_SuperScheme"//CHAR(0)
  ! mesh name  
  CHARACTER(LEN=1024) :: mesh = "tet.2.msh"//CHAR(0)

  ! test number 1, valid are { 1,...,5 }
  test = 1

  ! number of unknowns 
  n=5
  ! number of non-zero entries in the matrix 
  nnz=12

  ! allocate memory 
  allocate (rows(n+1), columns(nnz), Ax(nnz), rhs(n))

  ! choose format 
  CSRformat = 3 

  ! block size if your matrix contains dense blocks (e.g. for DG methods)
  ! if you're unsure select blockSize = 1 
  blockSize = 1

  ! setup system 
  !     We solve  A * x = b with 
  !
  !          | 2  3  0  0  0 |      | 1 |       |  8 |
  !          | 3  0  4  0  6 |      | 2 |       | 45 |
  !      A = | 0 -1 -3  2  0 |  x = | 3 |   b = | -3 |
  !          | 0  0  1  0  0 |      | 4 |       |  3 |
  !          | 0  4  2  0  1 |      | 5 |       | 19 |

  ! the row first index 
  rows(1) = 1
  rows(2) = 3
  rows(3) = 6
  rows(4) = 9
  rows(5) = 10
  rows(6) = nnz + 1

  ! real column numbers 
  columns(1)  = 1
  columns(2)  = 2
  columns(3)  = 1
  columns(4)  = 3
  columns(5)  = 5
  columns(6)  = 2
  columns(7)  = 3
  columns(8)  = 4
  columns(9)  = 3
  columns(10) = 2
  columns(11) = 3
  columns(12) = 5

  ! matrix entries
  Ax(1)  = 2.0d0
  Ax(2)  = 3.0d0
  Ax(3)  = 3.0d0
  Ax(4)  = 4.0d0
  Ax(5)  = 6.0d0
  Ax(6)  = -1.0d0
  Ax(7)  = -3.0d0
  Ax(8)  = 2.0d0
  Ax(9)  = 1.0d0
  Ax(10) = 4.0d0
  Ax(11) = 2.0d0
  Ax(12) = 1.0d0

  ! right hand side
  rhs(1) = 8.0d0
  rhs(2) = 45.0d0
  rhs(3) = -3.0d0
  rhs(4) = 3.0d0
  rhs(5) = 19.0d0

  ! call of writebenchmarkfile method to write data in correct way 
  call writebenchmarkfile(path, nameAndScheme, test, mesh, CSRformat, blockSize, n, n, rows, columns, Ax, rhs)

end program writetest
