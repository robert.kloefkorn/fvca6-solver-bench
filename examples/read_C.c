#include <stdlib.h>
#include <math.h>
#include "writexdr.h"

/********************************************************************
 *
 * C program writing example file
 *
 ********************************************************************/
int main()
{
  /* setup system
     We read the solution of A * x = b with

          | 2  3  0  0  0 |      | 1 |       |  8 |
          | 3  0  4  0  6 |      | 2 |       | 45 |
      A = | 0 -1 -3  2  0 |  x = | 3 |   b = | -3 |
          | 0  0  1  0  0 |      | 4 |       |  3 |
          | 0  4  2  0  1 |      | 5 |       | 19 |
  */

  /* generated with the call `benchruntime c.xdr' */
  const char* filename = "c.xdr.output.xdr";

  const int n = 5 ; /* we know this a-priori since in our programs we
                       already have the discretization grid */

  double x [5] = { 0.0, 0.0, 0.0, 0.0, 0.0 } ;
  const double u[ 5 ]= { 1.0, 2.0, 3.0, 4.0, 5.0 };

  int i;

  /* call write function, see writexdr.h and writexdr.c for docu
   * x needs to be pre-allocated with the size to be read */
  readSolution( filename, n, x );

  for( i=0; i<n; ++i )
  {
    if( fabs( x[ i ] - u[ i ] ) > 1e-12 )
    {
      fprintf(stderr,"ERROR: solution read (x[ %d ] = %f) is wrong, should be %f !",i,x[i],u[i]);
    }
    printf("x[ %d ] = %f \n",i,x[i]);
  }

  return 0;
}
