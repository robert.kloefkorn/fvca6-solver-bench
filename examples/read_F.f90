!------------------------------------------------------------------
!
! This in an example how to use the writebenchmarkfile function in a
! Fortran code. The file writexdr.o has to be linked against the
! Fortran program.
!
!------------------------------------------------------------------


! extern C routines are included as modules 

module readSolutionFile
  interface

    ! read solution from file, see the files writexdr.h and writexdr.c for docu 
    subroutine readsolution(filename, nunknowns, solution)

    ! change of these values is not necessary 
    ! dynamically allocatable vectors can be used, see below 
    PARAMETER(n=5)
    character filename 
    integer nunknowns
    double precision solution (n)

    end subroutine
  end interface
end module

!--------------------------------------------------------
! main program 
!--------------------------------------------------------

program readtest 
use readSolutionFile

  CHARACTER(LEN=48) :: filename = "f90.xdr.output.xdr"//CHAR(0)

  integer n

  double precision, allocatable, dimension (:):: x, u

  ! number of unknowns 
  n=5

  ! allocate memory 
  allocate (x(n), u(n))

  ! setup system 
  !     We solve  A * x = b with 
  !
  !          | 2  3  0  0  0 |      | 1 |       |  8 |
  !          | 3  0  4  0  6 |      | 2 |       | 45 |
  !      A = | 0 -1 -3  2  0 |  x = | 3 |   b = | -3 |
  !          | 0  0  1  0  0 |      | 4 |       |  3 |
  !          | 0  4  2  0  1 |      | 5 |       | 19 |

  ! the exact solution 
  u(1) = 1.0d0
  u(2) = 2.0d0
  u(3) = 3.0d0
  u(4) = 4.0d0
  u(5) = 5.0d0

  ! call of writebenchmarkfile method to write data in correct way 
  ! x needs to be pre-allocated with the correct size 
  call readsolution( filename, n, x) 

  DO j=1, n 
    u(j) = u(j) - x(j)
    IF (abs( u(j) ) > 0.000000000001d0) then 
      print *,"ERORR: ", u(j)
    else 
      print *,"x[ ", j, " ] = ", x(j)
    endif  
  ENDDO

end program readtest
