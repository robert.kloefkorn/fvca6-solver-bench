/////////////////////////////////////////////
//
//  Musterloesung Blatt 3, Aufgabe 3
//
////////////////////////////////////////////
#include <iostream>
#include <cmath>
#include <rpc/xdr.h>

using namespace std;

const int n = 100; // Dimension des Gleichungssystems

// Hilbert Matrix
void setup(double A[n][n], double b[n], double x[n] )
{
  for(int i=0; i<n; i++)
  {
    b[i] = 1.0;
    x[i] = 0.0;
    for(int j=0; j<n; j++)
    {
      A[i][j] = 0.0;
    }
  }
  A[0][0] = 1.0;
  A[n-1][n-1] = 1.0;
  x[0] = 1.0;
  x[n-1] = 1.0;
  for(int i=1; i<n-1; i++)
  {
    A[i][i] = 2.0;
    for(int j=0; j<n; j++)
    {
      if( j == (i-1) || j == (i+1) ) A[i][j] = -1.0;
    }
  }
}


// LU Zerlegung einer Matrix a
//
// in:  Matrix a
// out: LU Zerlegung LU = A, L und U gespeicehrt in a
void LU_decomposition(double a[n][n])
{
  for(int i=0; i<n-1; ++i)
  {
    // Elimination
    for(int k=i+1; k<n; ++k)
    {
      const double lambda = a[k][i] / a[i][i];
      a[k][i] = lambda;
      for(int j=i+1; j<n; ++j) a[k][j] -= lambda * a[i][j];
    }
  }

  /*
  cout << "LU Zerlegung = "  << endl;
  // Ausgabe der LU Zerlegung
  for(int i=0; i<n-1; ++i)
  {
    for(int j=0; j<n-1; ++j)
    {
      cout << a[i][j] << " ";
    }
    cout << endl;
  }
  */
}

// solve Ax = b <=> LUx = b
// in:  LU Zerlegung von A und Permutationsmatrix gespeichert in a,
//      Rechte Seite gespeichert in b
// out: Loesung x
void LU_solve(const double a[n][n], const double b[n],
              double x[n])
{
  // 0. x = b
  for(int i=0; i<n; ++i) x[i] = b[i];

  // 1. Lx = x_old, vorwaerts loesen
  for(int i=0; i<n; ++i)
  {
    double dot = 0.0;
    for(int j=0; j<i; j++) dot += a[i][j] * x[j];
    x[i] -= dot;
  }

  // 2. Ux = x_old, rueckwaerts loesen
  for(int i=n-1; i>=0; --i)
  {
    double dot = 0.0;
    for(int j=i+1; j<n; j++) dot += a[i][j] * x[j];
    x[i] = (x[i] - dot) / a[i][i];
  }

}


int main()
{
  double A[n][n], b[n], x[n];

  // setup Matrix
  setup(A, b, x);

  // LU Zerlegung berechnen
  LU_decomposition(A);

  // loesen
  LU_solve(A, b, x);

  // Output
  cout << "Ergebnis: " << endl;
  for(int i=0; i<n; ++i)
  {
    // rechnet x[i] - (-1)^i
    cout << x[i] << endl;
  }

  FILE* file = fopen("solution.xdr", "wb" );
  XDR xdrs;
  xdrstdio_create( &xdrs, file, XDR_ENCODE);

  int n1 = n;
  xdr_int( &xdrs, &n1 );
  for(int i=0; i<n; ++i)
    xdr_double( &xdrs, &x[i]);

  /* destroy XDR stream */
  xdr_destroy( &xdrs );
  fclose( file );
  /*
  cout << "Fehler: " << endl;
  for(int i=0; i<n; ++i)
  {
    // rechnet x[i] - (-1)^i
    cout << abs(x[i]- pow( -1.0, i ) ) << endl;
  }
  */

  return 0;
}
