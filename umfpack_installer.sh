#!/bin/bash 

# versions used 
UMFVERSION=5.4.0
AMDVERSION=2.2.1
UFCONFVERSION=3.5.0

if test "x$1" = "x" ; then 
STDFLAGS="-O3 -funroll-loops -finline-functions" 
else 
STDFLAGS="$1" 
fi 

echo "Installing UMFPACK with CFLAGS = $STDFLAGS" 

sleep 2 

# get old directory 
OLDDIR=`pwd`

INSTALLDIR="./UMFPACK"

# got to install dir 
mkdir $INSTALLDIR 
cd $INSTALLDIR

# get sources (use this version for all tests, current version 2010/04) 
wget http://www.cise.ufl.edu/research/sparse/UFconfig/UFconfig-$UFCONFVERSION.tar.gz 
wget http://www.cise.ufl.edu/research/sparse/amd/AMD-$AMDVERSION.tar.gz
wget http://www.cise.ufl.edu/research/sparse/umfpack/UMFPACK-$UMFVERSION.tar.gz 

# unpack 
tar zxvf UFconfig-$UFCONFVERSION.tar.gz
tar zxvf AMD-$AMDVERSION.tar.gz
tar zxvf UMFPACK-$UMFVERSION.tar.gz

# set configurations 
cd UFconfig 

# use GNU compiler 
echo "CC = gcc" >> UFconfig.mk
# use this CFLAGS 
echo "CFLAGS = $STDFLAGS" >> UFconfig.mk

# don't link to libg2c (deprecated)
echo "BLAS = -lblas -lgfortran -lgfortranbegin" >> UFconfig.mk

cd ../AMD/
make 

cd ../UMFPACK/
make 

cd $OLDDIR
